/*!
Utility functions
*/
use std::cmp::Ordering;
use Ordering::*;

/// Compare two booleans in a constant way
#[inline]
pub const fn bool_cmp(left: bool, right: bool) -> Ordering {
    match (left, right) {
        (true, false) => Greater,
        (false, true) => Less,
        _ => Equal,
    }
}

/// Join two orderings to form a lattice
#[inline]
pub const fn lattice_ord(left: Ordering, right: Ordering) -> Option<Ordering> {
    match (left, right) {
        (Equal, right) => Some(right),
        (left, Equal) => Some(left),
        (Greater, Greater) => Some(Greater),
        (Less, Less) => Some(Less),
        (Greater, Less) | (Less, Greater) => None,
    }
}

/// Join two optional orderings to form a lattice
#[inline]
pub const fn lattice_ord_opt(left: Option<Ordering>, right: Option<Ordering>) -> Option<Ordering> {
    match (left, right) {
        (Some(left), Some(right)) => lattice_ord(left, right),
        _ => None,
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn bool_cmp_test() {
        let bools = [true, false];
        for &l in &bools {
            for &r in &bools {
                assert_eq!(bool_cmp(l, r), l.cmp(&r))
            }
        }
    }
    #[test]
    fn lattice_ord_test() {
        let tuples = [
            (Less, Less, Some(Less)),
            (Less, Equal, Some(Less)),
            (Less, Greater, None),
            (Equal, Equal, Some(Equal)),
            (Equal, Greater, Some(Greater)),
            (Greater, Greater, Some(Greater)),
        ];
        for &(left, right, result) in tuples.iter() {
            assert_eq!(lattice_ord(left, right), result);
            assert_eq!(lattice_ord_opt(Some(left), Some(right)), result);
        }
        for &ord in &[Less, Equal, Greater] {
            assert_eq!(lattice_ord_opt(Some(ord), None), None);
            assert_eq!(lattice_ord_opt(None, Some(ord)), None);
        }
    }
}
