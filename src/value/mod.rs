/*!
`isotope` values
*/
use crate::error::*;
use crate::eval::*;
use crate::*;
use fxhash::FxHashMap as HashMap;
use lazy_static::lazy_static;
use num::BigUint;
use smallvec::SmallVec;
use std::cmp::Ordering;
use std::collections::hash_map::Entry;
use std::fmt::{self, Debug, Display, Formatter};
use std::hash::{Hash, Hasher};
use std::ops::*;

mod expr;
mod lambda;
mod lifetime;
mod pi;
mod primitive;
mod reference;
mod symbol;
mod universe;
mod value_enum;
mod value_trait;
mod variance;
pub use expr::*;
pub use lambda::*;
pub use lifetime::*;
pub use pi::*;
pub use primitive::*;
pub use reference::*;
pub use symbol::*;
pub use universe::*;
pub use value_enum::*;
pub use value_trait::*;
pub use variance::*;

/// A `rain` value
#[derive(Clone, Eq, PartialEq)]
#[repr(transparent)]
pub struct ValId(pub Arc<ValueEnum>);

impl ValId {
    /// Directly produce a new `ValId` from a `ValueEnum`
    #[inline]
    pub fn new_direct(value: ValueEnum) -> ValId {
        ValId(Arc::new(value))
    }
    /// Get this `ValId` as a pointer
    #[inline]
    pub fn as_ptr(&self) -> *const ValueEnum {
        Arc::as_ptr(&self.0)
    }
    /// Check whether this `ValId` is pointer-equal to another
    #[inline]
    pub fn ptr_eq(&self, other: &ValId) -> bool {
        self.as_ptr() == other.as_ptr()
    }
    /// Get this `ValId` as a `ValueEnum`
    #[inline]
    pub fn as_enum(&self) -> &ValueEnum {
        &self.0
    }
    /// Normalize this `ValId`, returning if any change occured
    #[inline]
    pub fn normalize(&mut self) -> bool {
        if let Some(normal) = self.normalized() {
            *self = normal.clone();
            true
        } else {
            false
        }
    }
    /// Get this `ValId`, but normalized
    #[inline]
    pub fn get_normalized(&self) -> &ValId {
        self.normalized().unwrap_or(self)
    }
    /// Convert an `&ValId` to an `&Option<ValId>`
    #[inline]
    pub fn to_opt(&self) -> &Option<ValId> {
        debug_assert_eq!(
            std::mem::size_of::<ValId>(),
            std::mem::size_of::<Option<ValId>>()
        );
        debug_assert_eq!(
            std::mem::align_of::<ValId>(),
            std::mem::align_of::<Option<ValId>>()
        );
        unsafe { &*(self as *const _ as *const Option<ValId>) }
    }
}

#[allow(clippy::derive_hash_xor_eq)]
impl Hash for ValId {
    #[inline]
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.code().hash(hasher)
    }
}

impl Debug for ValId {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        Debug::fmt(&self.0, fmt)
    }
}

impl Deref for ValId {
    type Target = ValueEnum;
    #[inline]
    fn deref(&self) -> &ValueEnum {
        &self.0
    }
}

impl Value for ValId {
    #[inline]
    fn is_ty(&self) -> bool {
        self.0.is_ty()
    }
    #[inline]
    fn is_kind(&self) -> bool {
        self.0.is_kind()
    }
    #[inline]
    fn is_groupoid(&self) -> bool {
        self.0.is_groupoid()
    }
    #[inline]
    fn is_instant(&self) -> bool {
        self.0.is_instant()
    }
    #[inline]
    fn elem_constraints(&self) -> Result<&Constraints, Error> {
        self.0.elem_constraints()
    }
    #[inline]
    fn elem_linearity(&self) -> Result<Linearity, Error> {
        self.0.elem_linearity()
    }
    #[inline]
    fn is_lifetime(&self) -> bool {
        self.0.is_lifetime()
    }
    #[inline]
    fn linearity(&self) -> Linearity {
        self.0.linearity()
    }
    #[inline]
    fn is_nonlinear(&self) -> bool {
        self.0.is_nonlinear()
    }
    #[inline]
    fn is_const(&self) -> bool {
        self.0.is_const()
    }
    #[inline]
    fn local_constraints(&self) -> &Constraints {
        self.0.local_constraints()
    }
    #[inline]
    fn instant_constraint(&self) -> Result<&Constraint, Error> {
        self.0.instant_constraint()
    }
    #[inline]
    fn ty(&self) -> &ValId {
        self.0.ty()
    }
    #[inline]
    fn into_enum(self) -> ValueEnum {
        self.as_enum().clone()
    }
    #[inline]
    fn into_valid(self) -> ValId {
        self
    }
    #[inline]
    fn fv(&self) -> &SymbolSet {
        self.0.fv()
    }
    #[inline]
    fn def_fv(&self) -> &SymbolSet {
        self.0.def_fv()
    }
    #[inline]
    fn contains(&self, value: &ValId) -> Result<Match, Error> {
        self.0.contains(value)
    }
    #[inline]
    fn subtype(&self, other: &ValId, variance: Variance) -> Result<Match, Error> {
        self.0.subtype(other, variance)
    }
    #[inline]
    fn try_apply_in_ctx(&self, arg: &ValId, ctx: &mut EvalCtx) -> Result<Application, Error> {
        self.0.try_apply_in_ctx(arg, ctx)
    }
    #[inline]
    fn apply_ty_in_ctx(&self, arg: &ValId, ctx: &mut EvalCtx) -> Result<Abstract, Error> {
        self.0.apply_ty_in_ctx(arg, ctx)
    }
    #[inline]
    fn apply_ty(&self, arg: &ValId) -> Result<Abstract, Error> {
        self.apply_ty_in_ctx(arg, &mut EvalCtx::default())
    }
    #[inline]
    fn eval_in_ctx(&self, ctx: &mut EvalCtx) -> Option<ValId> {
        if ctx.is_not_subst(self.fv()) {
            return None;
        }
        if let Some(cached) = ctx.cached_eval(self) {
            return Some(cached.clone());
        }
        let evaluated = self.0.eval_in_ctx(ctx);
        if let Some(eval) = &evaluated {
            ctx.cache_eval(self.clone(), eval.clone())
        }
        evaluated
    }
    #[inline]
    fn is_normal(&self) -> bool {
        self.0.is_normal()
    }
    #[inline]
    fn normalized(&self) -> Option<&ValId> {
        self.0.normalized()
    }
    #[inline]
    fn code(&self) -> u64 {
        self.0.code()
    }
    #[inline]
    fn normal_code(&self) -> u64 {
        self.0.normal_code()
    }
}

/// Utilities for testing `Value` implementations and similar
pub mod test {
    use super::*;

    /// Perform basic consistency checks on the `Value` trait implementation for an object.
    pub fn basic_value_consistency<V: Debug + Value>(value: V) {
        // Basic construction
        let into_valid = value.clone().into_valid();
        let into_enum = value.clone().into_enum();
        assert_eq!(into_enum, *into_valid.as_enum());

        // Type consistency
        let ty = value.ty();
        assert_eq!(ty, into_valid.ty());
        assert_eq!(ty, into_enum.ty());

        // Linearity consistency
        let ty_lin = value.elem_linearity().ok();
        assert_eq!(ty_lin, into_valid.elem_linearity().ok());
        assert_eq!(ty_lin, into_enum.elem_linearity().ok());
        let is_ty = value.is_ty();
        assert_eq!(is_ty, into_valid.is_ty());
        assert_eq!(is_ty, into_enum.is_ty());
        assert_eq!(is_ty, ty_lin.is_some());

        // Linearity consistency
        let lin = value.linearity();
        assert_eq!(lin, into_valid.linearity());
        assert_eq!(lin, into_enum.linearity());

        // Constraint consistency
        let locals = value.local_constraints();
        assert_eq!(locals, into_valid.local_constraints());
        assert_eq!(locals, into_enum.local_constraints());

        // Normalization consistency
        let is_normal = value.is_normal();
        let normalized = value.normalized();
        assert_eq!(normalized, into_valid.normalized());
        assert_eq!(normalized, into_enum.normalized());
        match (is_normal, normalized) {
            (true, Some(normalized)) => panic!(
                "Value {:?} is claimed normal, but normalizes to {:?}",
                value, normalized
            ),
            (false, None) => panic!(
                "Value {:?} is claimed non-normal, but does not normalize",
                value
            ),
            (false, Some(normalized)) => assert_ne!(into_valid, *normalized),
            _ => {}
        }
    }
}
