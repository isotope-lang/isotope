/*!
The `ValueEnum` and associated impls
*/
use super::*;

/// The enumeration of possible `isotope` values
#[derive(Clone, Eq, PartialEq, Hash)]
pub enum ValueEnum {
    // === Basic expressions ===
    /// A symbol
    Symbol(Symbol),
    /// An S-expression
    Sexpr(Sexpr),
    /// A lambda abstraction
    Lambda(Lambda),
    /// A dependent function type
    Pi(Pi),

    // === Type theory ===
    /// Basic typing universes
    Universe(Universe),

    // === Instants ===
    /// A reference type
    Reference(Reference),
    /// A borrowed value
    Borrowed(Borrowed),
    /// A lifetime
    Lifetime(Lifetime),
    /// The instant at infinity
    Infty(Infty),
    /// The instant at zero
    Zero(Zero),

    // === Primitive values ===
    /// The empty type
    Empty(Empty),
    /// The unit type
    Unit(Unit),
    /// The unique value of the unit type
    Nil(Nil),
    /// A boolean constant
    Boolean(bool),
    /// The type of booleans
    Bool(Bool),
    /// A natural number
    Natural(BigUint),
    /// The type of natural numbers
    Nat(Nat),
}

/// Perform the same action for every member of the `ValueEnum`
#[macro_export]
macro_rules! forv {
    ($v:expr; $i:ident => $e:expr) => {
        match $v {
            ValueEnum::Symbol($i) => $e,
            ValueEnum::Sexpr($i) => $e,
            ValueEnum::Lambda($i) => $e,
            ValueEnum::Pi($i) => $e,

            ValueEnum::Universe($i) => $e,

            ValueEnum::Reference($i) => $e,
            ValueEnum::Borrowed($i) => $e,
            ValueEnum::Lifetime($i) => $e,
            ValueEnum::Infty($i) => $e,
            ValueEnum::Zero($i) => $e,

            ValueEnum::Empty($i) => $e,
            ValueEnum::Unit($i) => $e,
            ValueEnum::Nil($i) => $e,
            ValueEnum::Boolean($i) => $e,
            ValueEnum::Bool($i) => $e,
            ValueEnum::Natural($i) => $e,
            ValueEnum::Nat($i) => $e,
        }
    };
}

impl Debug for ValueEnum {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        forv!(self; x => Debug::fmt(x, fmt))
    }
}

impl Value for ValueEnum {
    #[inline]
    fn is_ty(&self) -> bool {
        forv!(self; x => x.is_ty())
    }
    #[inline]
    fn is_groupoid(&self) -> bool {
        forv!(self; x => x.is_groupoid())
    }
    #[inline]
    fn is_kind(&self) -> bool {
        forv!(self; x => x.is_kind())
    }
    #[inline]
    fn is_instant(&self) -> bool {
        forv!(self; x => x.is_instant())
    }
    #[inline]
    fn elem_linearity(&self) -> Result<Linearity, Error> {
        forv!(self; x => x.elem_linearity())
    }
    #[inline]
    fn elem_constraints(&self) -> Result<&Constraints, Error> {
        forv!(self; x => x.elem_constraints())
    }
    #[inline]
    fn is_lifetime(&self) -> bool {
        forv!(self; x => x.is_lifetime())
    }
    #[inline]
    fn linearity(&self) -> Linearity {
        forv!(self; x => x.linearity())
    }
    #[inline]
    fn is_nonlinear(&self) -> bool {
        forv!(self; x => x.is_nonlinear())
    }
    #[inline]
    fn is_const(&self) -> bool {
        let is_const = forv!(self; x => x.is_const());
        if is_const {
            debug_assert_eq!(*self.fv(), SymbolSet::EMPTY);
        }
        is_const
    }
    #[inline]
    fn local_constraints(&self) -> &Constraints {
        forv!(self; x => x.local_constraints())
    }
    #[inline]
    fn instant_constraint(&self) -> Result<&Constraint, Error> {
        forv!(self; x => x.instant_constraint())
    }
    #[inline]
    fn contains(&self, value: &ValId) -> Result<Match, Error> {
        forv!(self; x => x.contains(value))
    }
    #[inline]
    fn subtype(&self, other: &ValId, variance: Variance) -> Result<Match, Error> {
        forv!(self; x => x.subtype(other, variance))
    }
    #[inline]
    fn apply_in_ctx(&self, arg: &ValId, ctx: &mut EvalCtx) -> Result<ValId, Error> {
        forv!(self; x => x.apply_in_ctx(arg, ctx))
    }
    #[inline]
    fn try_apply_in_ctx(&self, arg: &ValId, ctx: &mut EvalCtx) -> Result<Application, Error> {
        forv!(self; x => x.try_apply_in_ctx(arg, ctx))
    }
    #[inline]
    fn eval_in_ctx(&self, ctx: &mut EvalCtx) -> Option<ValId> {
        forv!(self; x => x.eval_in_ctx(ctx))
    }
    #[inline]
    fn apply(&self, arg: &ValId) -> Result<ValId, Error> {
        forv!(self; x => x.apply(arg))
    }
    #[inline]
    fn try_apply(&self, arg: &ValId) -> Result<Application, Error> {
        forv!(self; x => x.try_apply(arg))
    }
    #[inline]
    fn apply_ty_in_ctx(&self, arg: &ValId, ctx: &mut EvalCtx) -> Result<Abstract, Error> {
        forv!(self; x => x.apply_ty_in_ctx(arg, ctx))
    }
    #[inline]
    fn apply_ty(&self, arg: &ValId) -> Result<Abstract, Error> {
        forv!(self; x => x.apply_ty(arg))
    }
    #[inline]
    fn ty(&self) -> &ValId {
        forv!(self; x => x.ty())
    }
    #[inline]
    fn into_enum(self) -> ValueEnum {
        self
    }
    #[inline]
    fn into_valid(self) -> ValId {
        forv!(self; x => x.into_valid())
    }
    #[inline]
    fn fv(&self) -> &SymbolSet {
        forv!(self; x => x.fv())
    }
    #[inline]
    fn def_fv(&self) -> &SymbolSet {
        forv!(self; x => x.def_fv())
    }
    #[inline]
    fn is_normal(&self) -> bool {
        forv!(self; x => x.is_normal())
    }
    #[inline]
    fn normalized(&self) -> Option<&ValId> {
        if let Some(normal_form) = forv!(self; x => x.normalized()) {
            debug_assert!(!self.is_normal());
            Some(normal_form)
        } else {
            None
        }
    }
    #[inline]
    fn code(&self) -> u64 {
        forv!(self; x => x.code())
    }
    #[inline]
    fn normal_code(&self) -> u64 {
        let code = forv!(self; x => x.normal_code());
        {
            #[cfg(debug)]
            {
                if let Some(normal) = self.normalized() {
                    debug_assert_eq!(normal.code(), code)
                } else {
                    debug_assert_eq!(self.code(), code)
                }
            }
        }
        code
    }
}
