use super::*;

/// The variance of a type binding
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum Variance {
    /// This parameter is bivariant
    Bivariant,
    /// This parameter is covariant
    Covariant,
    /// This parameter is contravariant
    Contravariant,
    /// This parameter is invariant
    Invariant,
}

/// A list of all possible variances
pub const VARIANCES: [Variance; 4] = [Bivariant, Covariant, Contravariant, Invariant];

pub use Variance::*;

impl Default for Variance {
    #[inline]
    fn default() -> Variance {
        Bivariant
    }
}

impl Variance {
    /// Check whether two variances are equal, in a constant way
    #[inline]
    pub const fn const_eq(self, other: Variance) -> bool {
        matches!(
            (self, other),
            (Bivariant, Bivariant)
                | (Covariant, Covariant)
                | (Contravariant, Contravariant)
                | (Invariant, Invariant)
        )
    }
    /// Whether this variance matches an ordering
    #[inline]
    pub const fn matches_ord(self, ord: Ordering) -> bool {
        use Ordering::*;
        matches!(
            (self, ord),
            (Bivariant, _) | (_, Equal) | (Covariant, Less) | (Contravariant, Greater)
        )
    }
    /// "Multiply" a variance
    #[inline]
    pub const fn multiply(self, other: Variance) -> Variance {
        match (self, other) {
            (Invariant, _) => Invariant,
            (_, Invariant) => Invariant,
            (Bivariant, _) => Bivariant,
            (_, Bivariant) => Bivariant,
            (v, Covariant) => v,
            (Covariant, Contravariant) => Contravariant,
            (Contravariant, Contravariant) => Covariant,
        }
    }
    /// Flip a variance
    #[inline]
    pub const fn flip(self) -> Variance {
        match self {
            Covariant => Contravariant,
            Contravariant => Covariant,
            v => v,
        }
    }
    /// Take the meet of two variances
    #[inline]
    pub const fn meet(self, other: Variance) -> Variance {
        match (self, other) {
            (Invariant, var) | (var, Invariant) => var,
            _ if self.const_eq(other) => self,
            _ => Bivariant,
        }
    }
    /// Take the join of two variances
    #[inline]
    pub const fn join(self, other: Variance) -> Variance {
        match (self, other) {
            (Bivariant, var) | (var, Bivariant) => var,
            _ if self.const_eq(other) => self,
            _ => Invariant,
        }
    }
    /// Take a given variance with respect to a given symbol
    #[inline]
    pub fn of<T: Typed>(self, symbol: &T) -> Variance {
        if symbol.get_ty().is_groupoid() {
            Bivariant
        } else {
            self
        }
    }
    /// Whether a variance is directed
    #[inline]
    pub const fn is_directed(&self) -> bool {
        match self {
            Variance::Covariant => true,
            Variance::Contravariant => true,
            Variance::Invariant => false,
            Variance::Bivariant => false,
        }
    }
    /// Compare two variances in a constant way
    #[inline]
    pub const fn const_cmp(self, other: Variance) -> Option<Ordering> {
        use Ordering::*;
        match (self, other) {
            _ if self.const_eq(other) => Some(Equal),
            (Invariant, _) => Some(Greater),
            (_, Invariant) => Some(Less),
            (Bivariant, _) => Some(Less),
            (_, Bivariant) => Some(Greater),
            _ => None,
        }
    }
    /// Get a code for this variance
    #[inline]
    pub const fn code(self) -> u64 {
        self as u64
    }
}

impl PartialOrd for Variance {
    #[inline]
    fn partial_cmp(&self, other: &Variance) -> Option<Ordering> {
        self.const_cmp(*other)
    }
}

impl BitAnd<Variance> for Variance {
    type Output = Variance;
    fn bitand(self, other: Variance) -> Variance {
        self.meet(other)
    }
}

impl BitOr<Variance> for Variance {
    type Output = Variance;
    fn bitor(self, other: Variance) -> Variance {
        self.join(other)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn flip() {
        assert_eq!(Covariant.flip(), Contravariant);
        assert_eq!(Contravariant.flip(), Covariant);
        assert_eq!(Invariant.flip(), Invariant);
        assert_eq!(Bivariant.flip(), Bivariant);
    }
    #[test]
    fn meet_join() {
        let tuples = [
            (Invariant, Invariant, Invariant, Invariant),
            (Invariant, Covariant, Covariant, Invariant),
            (Invariant, Contravariant, Contravariant, Invariant),
            (Invariant, Bivariant, Bivariant, Invariant),
            (Covariant, Covariant, Covariant, Covariant),
            (Covariant, Contravariant, Bivariant, Invariant),
            (Covariant, Bivariant, Bivariant, Covariant),
            (Contravariant, Contravariant, Contravariant, Contravariant),
            (Contravariant, Bivariant, Bivariant, Contravariant),
            (Bivariant, Bivariant, Bivariant, Bivariant),
        ];
        for &(left, right, meet, join) in tuples.iter() {
            assert_eq!(left.join(right), join);
            assert_eq!(right.join(left), join);
            assert_eq!(left.meet(right), meet);
            assert_eq!(right.meet(left), meet);
            assert!(join >= left);
            assert!(join >= right);
            assert!(left <= join);
            assert!(right <= join);
            assert!(meet <= left);
            assert!(meet <= right);
            assert!(left >= meet);
            assert!(right >= meet);
        }
    }
    #[test]
    fn partial_ord() {
        use Ordering::*;
        let triples = [
            (Invariant, Invariant, Some(Equal)),
            (Invariant, Covariant, Some(Greater)),
            (Invariant, Contravariant, Some(Greater)),
            (Invariant, Bivariant, Some(Greater)),
            (Covariant, Invariant, Some(Less)),
            (Covariant, Covariant, Some(Equal)),
            (Covariant, Contravariant, None),
            (Covariant, Bivariant, Some(Greater)),
            (Contravariant, Invariant, Some(Less)),
            (Contravariant, Covariant, None),
            (Contravariant, Contravariant, Some(Equal)),
            (Contravariant, Bivariant, Some(Greater)),
            (Bivariant, Invariant, Some(Less)),
            (Bivariant, Covariant, Some(Less)),
            (Bivariant, Contravariant, Some(Less)),
            (Bivariant, Bivariant, Some(Equal)),
        ];
        for (left, right, ordering) in triples.iter() {
            assert_eq!(left.partial_cmp(right), *ordering);
            assert_eq!(right.partial_cmp(left), ordering.map(Ordering::reverse))
        }
    }
    #[test]
    fn multiplication_commutes() {
        for &left in &VARIANCES {
            for &right in &VARIANCES {
                assert_eq!(left.multiply(right), right.multiply(left))
            }
        }
    }
}
