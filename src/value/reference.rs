/*!
Reference types and borrows
*/

use super::*;

/// Reference types
#[derive(Debug, Clone, Eq)]
pub struct Reference {
    /// The referenced type
    ty: ValId,
    /// The constraints values of this reference type must satisfy
    constraints: Constraints,
    /// The free variable set of this reference set
    fv: SymbolSet,
    /// The code of this reference
    code: u64,
}

impl Reference {
    /// Create a new static reference type for a given
    pub fn static_ref(ty: ValId) -> Result<Reference, Error> {
        Self::try_new(ty, &Constraint::default())
    }
    /// Create a new reference with a given type and constraint.
    ///
    /// Return an error if the provided value is not actually a type
    pub fn try_new(ty: ValId, constraint: &Constraint) -> Result<Reference, Error> {
        let mut constraints = ty.elem_constraints()?.clone();
        constraints.rebase_require(Some(ty.clone()), constraint, None, false);
        let mut fv = constraints.fv();
        fv.insert_set(ty.fv());
        let mut result = Reference {
            ty,
            constraints,
            fv,
            code: 0,
        };
        result.fix_code();
        Ok(result)
    }
    /// Get the hasher used for references
    pub fn get_hasher() -> AHasher {
        AHasher::new_with_keys(5452, 1134)
    }
    /// Fix the code of this reference
    fn fix_code(&mut self) {
        let mut hasher = Self::get_hasher();
        self.hash(&mut hasher);
        self.code = hasher.finish()
    }
}

impl PartialEq for Reference {
    fn eq(&self, other: &Reference) -> bool {
        self.ty == other.ty && self.constraints == other.constraints
    }
}

impl Hash for Reference {
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.ty.hash(hasher);
        self.constraints.hash(hasher);
    }
}

impl Value for Reference {
    #[inline]
    fn is_ty(&self) -> bool {
        true
    }
    #[inline]
    fn elem_constraints(&self) -> Result<&Constraints, Error> {
        Ok(&self.constraints)
    }
    #[inline]
    fn elem_linearity(&self) -> Result<Linearity, Error> {
        Ok(Linearity::NONLINEAR)
    }
    #[inline]
    fn ty(&self) -> &ValId {
        self.ty.ty()
    }
    fn subtype(&self, other: &ValId, variance: Variance) -> Result<Match, Error> {
        match other.as_enum() {
            ValueEnum::Reference(other) => {
                if self.constraints == other.constraints {
                    self.ty.subtype(&other.ty, variance)
                } else {
                    unimplemented!("General constraint subtyping...")
                }
            }
            _ => Err(Error::TypeMismatch),
        }
    }
    #[inline]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Reference(self)
    }
    #[inline]
    fn code(&self) -> u64 {
        self.code
    }
}

/// A borrow of a value
#[derive(Debug, Clone, Eq)]
pub struct Borrowed {
    /// The borrowed value
    borrowed: ValId,
    /// The type of this borrowed value, which is always a reference
    ty: ValId,
    /// The free variable set of this borrowed value
    fv: SymbolSet,
    /// The code of this borrowed value
    code: u64,
}

impl PartialEq for Borrowed {
    fn eq(&self, other: &Borrowed) -> bool {
        self.borrowed == other.borrowed
    }
}

impl Hash for Borrowed {
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.borrowed.hash(hasher);
        self.code.hash(hasher);
    }
}

impl Borrowed {
    /// Create a new borrowed value
    pub fn new(borrowed: ValId) -> Borrowed {
        let mut constraint = Constraint::default();
        constraint
            .require(Some(borrowed.clone()), Relationship::EQ, true)
            .expect("Adding a requirement to a null constraint can never fail");
        let ty = Reference::try_new(borrowed.ty().clone(), &constraint)
            .expect("borrowed.ty() is a type")
            .into_valid();
        let fv = borrowed.fv().borrowed_deps();
        let mut result = Borrowed {
            borrowed,
            ty,
            fv,
            code: 0,
        };
        result.fix_code();
        result
    }
    /// Get the hasher used for borrowed values
    pub fn get_hasher() -> AHasher {
        AHasher::new_with_keys(5452, 1354)
    }
    /// Fix the code of this borrowed value
    fn fix_code(&mut self) {
        let mut hasher = Self::get_hasher();
        self.hash(&mut hasher);
        self.code = hasher.finish()
    }
}

impl Value for Borrowed {
    #[inline]
    fn is_ty(&self) -> bool {
        false
    }
    #[inline]
    fn ty(&self) -> &ValId {
        &self.ty
    }
    #[inline]
    fn fv(&self) -> &SymbolSet {
        &self.fv
    }
    #[inline]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Borrowed(self)
    }
    #[inline]
    fn code(&self) -> u64 {
        self.code
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn borrowing_constants_yields_static_references() {
        let static_bool = Reference::static_ref(BOOL.clone()).unwrap().into_valid();
        for &b in &[true, false] {
            let bb = Borrowed::new(b.into_valid());
            assert_eq!(*bb.ty(), static_bool)
        }
        let static_nat = Reference::static_ref(NAT.clone()).unwrap().into_valid();
        for n in 0..16 {
            let n = BigUint::new(vec![n]).into_valid();
            let bn = Borrowed::new(n);
            assert_eq!(*bn.ty(), static_nat)
        }
    }
    #[test]
    fn borrow_nat_function() {
        let n = SymbolId::param(NAT.clone()).unwrap();
        let nv = n.clone().into_valid();
        let mut nc = Constraint::new();
        nc.require(Some(nv.clone()), Relationship::EQ, true)
            .unwrap();
        let nb_ref = Reference::try_new(NAT.clone(), &nc).unwrap().into_valid();
        let bn = Borrowed::new(nv).into_valid();
        assert_eq!(*bn.ty(), nb_ref);
        let l = Lambda::try_new(n.clone(), bn).unwrap().into_valid();
        let p = Pi::try_new(
            n,
            nb_ref,
            FunctionalLinearity::new(
                Dependency {
                    variance: Covariant,
                    relationship: Relationship::EQ,
                },
                Linearity::NONLINEAR,
                Usage::OBSERVED,
            ),
        )
        .unwrap()
        .into_valid();
        assert_eq!(*l.ty(), p);
    }
}
