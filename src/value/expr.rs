/*!
S-expressions
*/

use super::*;
use once_cell::sync::OnceCell;

/// An S-expression
#[derive(Clone, Eq)]
pub struct Sexpr {
    /// The function being applied
    func: ValId,
    /// The argument it is being applied to
    arg: ValId,
    /// The cached type of this expression
    ty: ValId,
    /// The cached hash code of this expression
    code: u64,
    /// The free-variable set of this expression
    fv: SymbolSet,
    /// The constraint-set of this application
    constraints: Constraints,
    /// The normalized form of this S-expression
    normal_form: OnceCell<Option<ValId>>,
}

impl Sexpr {
    /// Try to construct a new S-expression
    pub fn try_new(func: ValId, arg: ValId) -> Result<Sexpr, Error> {
        let (ty, constraints) = func.ty().apply_ty(&arg)?.rectify(func.clone());
        let result = Self::new_unchecked(func, arg, ty, constraints);
        Ok(result)
    }
    /// Create a new, unchecked S-expression
    pub(super) fn new_unchecked(
        func: ValId,
        arg: ValId,
        ty: ValId,
        constraints: Constraints,
    ) -> Sexpr {
        let fv = func.fv().union(arg.fv());
        let mut hasher = Self::get_hasher();
        func.code().hash(&mut hasher);
        arg.code().hash(&mut hasher);
        let code = hasher.finish();
        Sexpr {
            func,
            arg,
            ty,
            code,
            fv,
            constraints,
            normal_form: OnceCell::new(),
        }
    }
    /// Get the hasher used for S-expressions
    #[inline]
    pub fn get_hasher() -> AHasher {
        AHasher::new_with_keys(312, 123)
    }
}

impl Value for Sexpr {
    #[inline]
    fn is_kind(&self) -> bool {
        false
    }
    #[inline]
    fn ty(&self) -> &ValId {
        &self.ty
    }
    #[inline]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Sexpr(self)
    }
    #[inline]
    fn code(&self) -> u64 {
        self.code
    }
    #[inline]
    fn fv(&self) -> &SymbolSet {
        &self.fv
    }
    #[inline]
    fn local_constraints(&self) -> &Constraints {
        &self.constraints
    }
    fn eval_in_ctx(&self, ctx: &mut EvalCtx) -> Option<ValId> {
        if ctx.is_null() {
            return None;
        }
        if ctx.is_empty() && ctx.is_normalizing() {
            return self.normalized().cloned();
        }
        let arg = self.arg.eval_in_ctx(ctx);
        let result = if ctx.is_normalizing() {
            self.func
                .apply_in_ctx(&self.arg, ctx)
                .expect("Applying substituted argument should never fail")
        } else {
            let func = self.func.eval_in_ctx(ctx);
            if func.is_none() && arg.is_none() {
                return None;
            }
            Sexpr::try_new(
                func.unwrap_or_else(|| self.func.clone()),
                arg.unwrap_or_else(|| self.arg.clone()),
            )
            .expect("Substituted sexpr construction should never fail")
            .into_valid()
        };
        Some(result)
    }
    fn try_apply_in_ctx(&self, arg: &ValId, ctx: &mut EvalCtx) -> Result<Application, Error> {
        if ctx.is_normalizing() {
            if let Some(normal) = self.eval_in_ctx(ctx) {
                return normal.try_apply_in_ctx(arg, ctx);
            }
        }
        Ok(Application::Abstract(None))
    }
    fn normalized(&self) -> Option<&ValId> {
        self.normal_form
            .get_or_init(|| {
                let (func, arg) = match (self.func.normalized(), self.arg.normalized()) {
                    (None, None) => match self
                        .func
                        .try_apply_norm(&self.arg)
                        .expect("Valid sexprs always yield a valid application")
                    {
                        Application::Abstract(_) => return None,
                        Application::Concrete(value) => {
                            debug_assert!(
                                value.is_normal(),
                                "Evaluated in normalizing context, so result should be normal!"
                            );
                            return Some(value);
                        }
                    },
                    (Some(func), None) => (func, &self.arg),
                    (None, Some(arg)) => (&self.func, arg),
                    (Some(func), Some(arg)) => (func, arg),
                };
                let result = Sexpr::new_unchecked(
                    func.clone(),
                    arg.clone(),
                    self.ty
                        .normalized()
                        .cloned()
                        .unwrap_or_else(|| self.ty.clone()),
                    self.constraints.clone(),
                );
                let result_valid = result.normalized().cloned();
                Some(result_valid.unwrap_or_else(|| result.into_valid()))
            })
            .as_ref()
    }
}

impl PartialEq for Sexpr {
    fn eq(&self, other: &Sexpr) -> bool {
        self.func == other.func && self.arg == other.arg
    }
}

impl Debug for Sexpr {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        fmt.debug_tuple("Sexpr")
            .field(&self.func)
            .field(&self.arg)
            .finish()
    }
}

impl Hash for Sexpr {
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.func.hash(hasher);
        self.arg.hash(hasher)
    }
}
