/*!
`isotope` typing universes
*/
use super::*;
use once_cell::sync::OnceCell;

/// A leveled typing universe
#[derive(Clone)]
pub struct Universe {
    /// The level of this typing universe
    level: u64,
    // The instant which must be less than or equal to the minimal end of all this universe's elements
    end: ValId,
    /// The (cached) hash-code of this typing universe
    code: u64,
    /// The type of this typing universe, as a cached `Arc`
    ty: OnceCell<ValId>,
}

impl Debug for Universe {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        match (self.level, self.end.as_enum()) {
            (0, ValueEnum::Zero(_)) => write!(fmt, "PROP"),
            (1, ValueEnum::Zero(_)) => write!(fmt, "SET"),
            (2, ValueEnum::Zero(_)) => write!(fmt, "TYPE"),
            _ => fmt
                .debug_tuple("Universe")
                .field(&self.level)
                .field(&self.end)
                .finish(),
        }
    }
}

lazy_static! {
    /// The universe of propositions
    pub static ref PROP: ValId = ValId::new_direct(ValueEnum::Universe(Universe::new_free(0)));
    /// The universe of sets
    pub static ref SET: ValId = ValId::new_direct(ValueEnum::Universe(Universe::new_free(1)));
    /// The universe of types
    pub static ref TYPE: ValId = ValId::new_direct(ValueEnum::Universe(Universe::new_free(2)));
}

impl Universe {
    /// Construct a new universe at a given level with a given lifetime constraint.
    fn new_unchecked(level: u64, end: ValId) -> Universe {
        let mut result = Universe {
            level,
            end,
            code: 0,
            ty: OnceCell::new(),
        };
        result.update_code();
        result
    }
    /// Construct a unconstrained universe at a given level
    pub fn new_free(level: u64) -> Universe {
        Self::new_unchecked(level, ZERO.clone())
    }
    /// Construct a maximally constrained universe at a given level
    pub fn new_static(level: u64) -> Universe {
        Self::new_unchecked(level, INFTY.clone())
    }
    /// Construct the successor universe to this universe
    pub fn construct_succ(&self) -> Universe {
        Self::new_unchecked(self.level + 1, self.end.clone())
    }
    /// Update the hash code of this typing universe
    ///
    /// Should be a no-op for any publically visibile universe
    pub fn update_code(&mut self) {
        let mut hasher = Self::get_hasher();
        self.level.hash(&mut hasher);
        self.end.code().hash(&mut hasher);
        self.code = hasher.finish();
    }
    /// Get the hasher used for universes
    #[inline]
    pub fn get_hasher() -> AHasher {
        AHasher::new_with_keys(2, 3)
    }
}

impl PartialEq for Universe {
    fn eq(&self, other: &Universe) -> bool {
        self.level == other.level
    }
}

impl Eq for Universe {}

impl Hash for Universe {
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.level.hash(hasher)
    }
}

impl Value for Universe {
    #[inline]
    fn is_ty(&self) -> bool {
        true
    }
    #[inline]
    fn is_groupoid(&self) -> bool {
        false
    }
    #[inline]
    fn is_kind(&self) -> bool {
        true
    }
    #[inline]
    fn ty(&self) -> &ValId {
        self.ty.get_or_init(|| self.construct_succ().into_valid())
    }
    #[inline]
    fn elem_linearity(&self) -> Result<Linearity, Error> {
        Ok(Linearity::NONLINEAR)
    }
    #[inline]
    fn contains(&self, other: &ValId) -> Result<Match, Error> {
        //TODO: *really* think about variance, here...
        match other.ty().as_enum() {
            ValueEnum::Universe(other) => {
                if self.level >= other.level {
                    Ok(Match::dependency(Usage::OBSERVED))
                } else {
                    Err(Error::UniverseLevelMismatch)
                }
            }
            _ => Err(Error::TypeMismatch),
        }
    }
    #[inline]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Universe(self)
    }
    #[inline]
    fn into_valid(self) -> ValId {
        match self.level {
            0 => PROP.clone(),
            1 => SET.clone(),
            2 => TYPE.clone(),
            _ => ValId::new_direct(ValueEnum::Universe(self)),
        }
    }
    #[inline]
    fn code(&self) -> u64 {
        self.code
    }
    #[inline]
    fn normal_code(&self) -> u64 {
        self.code
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn basic_universe_properties() {
        let prop = Universe::new_free(0);
        let set = prop.construct_succ();
        assert!(prop.is_ty());
        assert!(prop.is_kind());
        let prop_i = prop.clone().into_enum();
        let prop = ValueEnum::Universe(prop);
        assert_eq!(prop, prop_i);
        assert!(prop.is_ty());
        assert!(prop.is_kind());
        assert!(set.is_ty());
        assert!(set.is_kind());
        let set_i = set.clone().into_enum();
        let set = ValueEnum::Universe(set);
        assert_eq!(set, set_i);
        assert!(set.is_ty());
        assert!(set.is_kind());
        assert_eq!(prop.into_valid(), *PROP);
        assert_eq!(set.into_valid(), *SET);
    }
}
