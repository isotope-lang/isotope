use super::*;
use num::ToPrimitive;

/// The type of natural numbers
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Nat;

impl Nat {
    /// The code for the type of natural numbers
    pub const NAT_CODE: u64 = 0x4e4154;
    /// Get the hasher used for natural numbers
    #[inline]
    pub fn get_hasher() -> AHasher {
        AHasher::new_with_keys(0x4e41545552414c, 0x484153484552)
    }
}

impl Value for Nat {
    #[inline]
    fn is_ty(&self) -> bool {
        true
    }
    #[inline]
    fn elem_linearity(&self) -> Result<Linearity, Error> {
        Ok(Linearity::AFFINE)
    }
    #[inline]
    fn is_const(&self) -> bool {
        true
    }
    #[inline]
    fn ty(&self) -> &ValId {
        &*SET
    }
    fn subtype(&self, other: &ValId, _variance: Variance) -> Result<Match, Error> {
        match other.as_enum() {
            ValueEnum::Nat(_) => Ok(Match::dependency(Usage::USED).clip(other.linearity())),
            _ => Err(Error::TypeMismatch),
        }
    }
    #[inline]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Nat(self)
    }
    #[inline]
    fn into_valid(self) -> ValId {
        NAT.clone()
    }
    #[inline]
    fn code(&self) -> u64 {
        Self::NAT_CODE
    }
}

impl Value for BigUint {
    #[inline]
    fn ty(&self) -> &ValId {
        &*NAT
    }
    #[inline]
    fn linearity(&self) -> Linearity {
        Linearity::NONLINEAR
    }
    #[inline]
    fn is_const(&self) -> bool {
        true
    }
    #[inline]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Natural(self)
    }
    #[inline]
    fn into_valid(self) -> ValId {
        if let Some(small) = self.to_usize() {
            if small <= 0xF {
                return SMALL_NATS[small].clone();
            }
        }
        ValId::new_direct(ValueEnum::Natural(self))
    }
    #[inline]
    fn code(&self) -> u64 {
        let mut hasher = Nat::get_hasher();
        self.hash(&mut hasher);
        hasher.finish()
    }
}

lazy_static! {
    /// The type of naturals, as a `ValId`
    pub static ref NAT: ValId = ValId::new_direct(ValueEnum::Nat(Nat));
    /// The first 16 integers
    pub static ref SMALL_NATS: [ValId; 16] = [
        ValId::new_direct(BigUint::from(0x0u64).into_enum()),
        ValId::new_direct(BigUint::from(0x1u64).into_enum()),
        ValId::new_direct(BigUint::from(0x2u64).into_enum()),
        ValId::new_direct(BigUint::from(0x3u64).into_enum()),
        ValId::new_direct(BigUint::from(0x4u64).into_enum()),
        ValId::new_direct(BigUint::from(0x5u64).into_enum()),
        ValId::new_direct(BigUint::from(0x6u64).into_enum()),
        ValId::new_direct(BigUint::from(0x7u64).into_enum()),
        ValId::new_direct(BigUint::from(0x8u64).into_enum()),
        ValId::new_direct(BigUint::from(0x9u64).into_enum()),
        ValId::new_direct(BigUint::from(0xAu64).into_enum()),
        ValId::new_direct(BigUint::from(0xBu64).into_enum()),
        ValId::new_direct(BigUint::from(0xCu64).into_enum()),
        ValId::new_direct(BigUint::from(0xDu64).into_enum()),
        ValId::new_direct(BigUint::from(0xEu64).into_enum()),
        ValId::new_direct(BigUint::from(0xFu64).into_enum()),
    ];
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn nat_is_consistent() {
        value::test::basic_value_consistency(Nat);
    }

    #[test]
    fn small_nats_are_consistent() {
        for nat in SMALL_NATS.iter() {
            value::test::basic_value_consistency(nat.clone());
        }
    }

    #[test]
    fn basic_nat_properties() {
        assert!(Nat.is_ty());
        assert!(ValueEnum::Nat(Nat).is_ty());
        assert!(!Nat.is_kind());
        assert!(!ValueEnum::Nat(Nat).is_kind());
        assert_eq!(Nat.into_enum(), ValueEnum::Nat(Nat));
        assert_eq!(Nat.ty().as_ptr(), SET.as_ptr());
        assert_eq!(ValueEnum::Nat(Nat).ty().as_ptr(), SET.as_ptr());
    }
}
