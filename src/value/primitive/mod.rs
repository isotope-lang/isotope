/*!
Primitive `isotope` values and types
*/

use super::*;

mod logical;
mod natural;
pub use logical::*;
pub use natural::*;

/// The empty type
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Empty;

impl Empty {
    /// The code for the empty type
    pub const EMPTY_CODE: u64 = 0x454d5054590a;
}

/// The unit type
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Unit;

impl Unit {
    /// The code for the unit type
    pub const UNIT_CODE: u64 = 0x554e49540a;
}

/// The unique value of the unit type
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Nil;

impl Nil {
    /// The code for the value of the unit type
    pub const NIL_CODE: u64 = 0x4e494c;
}

lazy_static! {
    /// The empty type, as a `ValId`
    pub static ref EMPTY: ValId = ValId::new_direct(ValueEnum::Empty(Empty));
    /// The unit type, as a `ValId`
    pub static ref UNIT: ValId = ValId::new_direct(ValueEnum::Unit(Unit));
    /// The unique value of the unit type, as a `ValId`
    pub static ref NIL: ValId = ValId::new_direct(ValueEnum::Nil(Nil));
}

impl Value for Empty {
    #[inline]
    fn is_ty(&self) -> bool {
        true
    }
    #[inline]
    fn ty(&self) -> &ValId {
        &*PROP
    }
    #[inline]
    fn elem_linearity(&self) -> Result<Linearity, Error> {
        Ok(Linearity::NONLINEAR)
    }
    #[inline]
    fn is_const(&self) -> bool {
        true
    }
    fn subtype(&self, other: &ValId, _variance: Variance) -> Result<Match, Error> {
        match other.as_enum() {
            ValueEnum::Empty(_) => Ok(Match::dependency(Usage::OBSERVED)),
            _ => Err(Error::TypeMismatch),
        }
    }
    #[inline]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Empty(self)
    }
    #[inline]
    fn into_valid(self) -> ValId {
        EMPTY.clone()
    }
    #[inline]
    fn code(&self) -> u64 {
        Self::EMPTY_CODE
    }
    #[inline]
    fn normal_code(&self) -> u64 {
        Self::EMPTY_CODE
    }
}

impl Value for Unit {
    #[inline]
    fn is_ty(&self) -> bool {
        true
    }
    #[inline]
    fn ty(&self) -> &ValId {
        &*PROP
    }
    #[inline]
    fn elem_linearity(&self) -> Result<Linearity, Error> {
        Ok(Linearity::NONLINEAR)
    }
    #[inline]
    fn is_const(&self) -> bool {
        true
    }
    fn subtype(&self, other: &ValId, _variance: Variance) -> Result<Match, Error> {
        match other.as_enum() {
            ValueEnum::Unit(_) => Ok(Match::dependency(Usage::OBSERVED)),
            _ => Err(Error::TypeMismatch),
        }
    }
    #[inline]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Unit(self)
    }
    #[inline]
    fn into_valid(self) -> ValId {
        UNIT.clone()
    }
    #[inline]
    fn code(&self) -> u64 {
        Self::UNIT_CODE
    }
    #[inline]
    fn normal_code(&self) -> u64 {
        Self::UNIT_CODE
    }
}

impl Value for Nil {
    #[inline]
    fn is_ty(&self) -> bool {
        true
    }
    #[inline]
    fn ty(&self) -> &ValId {
        &*UNIT
    }
    #[inline]
    fn is_const(&self) -> bool {
        true
    }
    #[inline]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Nil(self)
    }
    #[inline]
    fn into_valid(self) -> ValId {
        NIL.clone()
    }
    #[inline]
    fn code(&self) -> u64 {
        Self::NIL_CODE
    }
    #[inline]
    fn normal_code(&self) -> u64 {
        Self::NIL_CODE
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn basic_bool_properties() {
        for b in [true, false].iter().copied() {
            assert!(!b.is_ty());
            assert!(!b.is_kind());
            assert!(!b.into_enum().is_ty());
            assert!(!b.into_enum().is_kind());
            assert_eq!(b.into_enum(), ValueEnum::Boolean(b));
            let manual = ValId::new_direct(ValueEnum::Boolean(b));
            let auto = b.into_valid();
            assert_eq!(auto, manual);
            assert_ne!(auto.as_ptr(), manual.as_ptr());
            assert_eq!(auto.as_ptr(), auto.as_ptr());
            assert_eq!(manual.as_ptr(), manual.as_ptr());
            assert_eq!(auto.as_ptr(), b.into_valid().as_ptr());
            assert!(!auto.ptr_eq(&manual));
            assert_eq!(b.ty().as_ptr(), BOOL.as_ptr());
            assert_eq!(auto.ty().as_ptr(), BOOL.as_ptr());
        }
        assert!(Bool.is_ty());
        assert!(ValueEnum::Bool(Bool).is_ty());
        assert!(!Bool.is_kind());
        assert!(!ValueEnum::Bool(Bool).is_kind());
        assert_eq!(Bool.into_enum(), ValueEnum::Bool(Bool));
        assert_eq!(Bool.ty().as_ptr(), SET.as_ptr());
        assert_eq!(ValueEnum::Bool(Bool).ty().as_ptr(), SET.as_ptr());
    }
    #[test]
    fn basic_primitive_codes() {
        assert_eq!(Bool.code(), Bool::BOOL_CODE);
        assert_eq!(true.code(), Bool::TRUE_CODE);
        assert_eq!(false.code(), Bool::FALSE_CODE);
        assert_eq!(Nat.code(), Nat::NAT_CODE);
        assert_eq!(Nil.code(), Nil::NIL_CODE);
        assert_eq!(Unit.code(), Unit::UNIT_CODE);
        assert_eq!(Empty.code(), Empty::EMPTY_CODE);
        assert_eq!(Bool.into_valid().code(), Bool::BOOL_CODE);
        assert_eq!(true.into_valid().code(), Bool::TRUE_CODE);
        assert_eq!(false.into_valid().code(), Bool::FALSE_CODE);
        assert_eq!(Nat.into_valid().code(), Nat::NAT_CODE);
        assert_eq!(Nil.into_valid().code(), Nil::NIL_CODE);
        assert_eq!(Unit.into_valid().code(), Unit::UNIT_CODE);
        assert_eq!(Empty.into_valid().code(), Empty::EMPTY_CODE);
    }
}
