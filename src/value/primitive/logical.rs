use super::*;

/// The type of booleans
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Bool;

impl Bool {
    /// The code for the boolean type
    pub const BOOL_CODE: u64 = 0x424f4f4c;
    /// The code for the constant `true`
    pub const TRUE_CODE: u64 = 0x54525545;
    /// The code for the constant `false`
    pub const FALSE_CODE: u64 = 0x46414c5345;
}

impl Value for Bool {
    #[inline]
    fn is_ty(&self) -> bool {
        true
    }
    #[inline]
    fn elem_linearity(&self) -> Result<Linearity, Error> {
        Ok(Linearity::NONLINEAR)
    }
    #[inline]
    fn ty(&self) -> &ValId {
        &*SET
    }
    #[inline]
    fn is_const(&self) -> bool {
        true
    }
    fn subtype(&self, other: &ValId, _variance: Variance) -> Result<Match, Error> {
        match other.as_enum() {
            ValueEnum::Bool(_) => Ok(Match::dependency(Usage::OBSERVED)),
            _ => Err(Error::TypeMismatch),
        }
    }
    #[inline]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Bool(self)
    }
    #[inline]
    fn into_valid(self) -> ValId {
        BOOL.clone()
    }
    #[inline]
    fn code(&self) -> u64 {
        Self::BOOL_CODE
    }
    #[inline]
    fn normal_code(&self) -> u64 {
        Self::BOOL_CODE
    }
}

impl Value for bool {
    #[inline]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Boolean(self)
    }
    #[inline]
    fn ty(&self) -> &ValId {
        &*BOOL
    }
    #[inline]
    fn elem_linearity(&self) -> Result<Linearity, Error> {
        Ok(Linearity::NONLINEAR)
    }
    #[inline]
    fn is_const(&self) -> bool {
        true
    }
    #[inline]
    fn into_valid(self) -> ValId {
        if self {
            TRUE.clone()
        } else {
            FALSE.clone()
        }
    }
    #[inline]
    fn code(&self) -> u64 {
        if *self {
            Bool::TRUE_CODE
        } else {
            Bool::FALSE_CODE
        }
    }
}

lazy_static! {
    /// The type of booleans, as a `ValId`
    pub static ref BOOL: ValId = ValId::new_direct(ValueEnum::Bool(Bool));
    /// The true boolean constant
    pub static ref TRUE: ValId = ValId::new_direct(ValueEnum::Boolean(true));
    /// The false boolean constant
    pub static ref FALSE: ValId = ValId::new_direct(ValueEnum::Boolean(false));
}
