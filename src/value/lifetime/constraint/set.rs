use super::*;

/// A set of constraints on a value being valid
#[derive(Clone, Eq, PartialEq, Hash)]
pub enum Constraints {
    /// A valid set of constraints
    Consistent(Consistent),
    /// A contradiction
    Contradiction,
}

impl Constraints {
    /// Create a new constraint set with a single dependency
    #[inline]
    pub fn dependency(dep: ValId) -> Constraints {
        let mut result = Constraints::default();
        result.require(
            None,
            Some(dep),
            Relationship::EQ.with_usage(Usage::USED),
            true,
        );
        result
    }
    /// Try to require a constraint between two values, returning an error on inconsistency
    pub fn try_require(
        &mut self,
        left: Option<ValId>,
        right: Option<ValId>,
        relationship: Relationship,
        separating: bool,
    ) -> Result<bool, Error> {
        match self {
            Constraints::Consistent(c) => c.require(left, right, relationship, separating),
            Constraints::Contradiction => Err(Error::Contradiction),
        }
    }
    /// Require a rebased constraint on a value, returning an error on inconsistency
    pub fn try_rebase_require(
        &mut self,
        value: Option<ValId>,
        constraint: &Constraint,
        base: Option<&ValId>,
        separating: bool,
    ) -> Result<bool, Error> {
        match self {
            Constraints::Consistent(c) => c.rebase_require(value, constraint, base, separating),
            Constraints::Contradiction => Err(Error::Contradiction),
        }
    }
    /// Clear a constraint set
    #[inline]
    pub fn clear(&mut self) {
        match self {
            Constraints::Consistent(c) => c.clear(),
            Constraints::Contradiction => *self = NO_CONSTRAINTS,
        }
    }
    /// Check whether this constraint set is consistent
    #[inline]
    pub fn is_consistent(&self) -> bool {
        matches!(self, Constraints::Consistent(_))
    }
    /// Check whether this constraint set is a contradiction
    #[inline]
    pub fn is_contradiction(&self) -> bool {
        matches!(self, Constraints::Contradiction)
    }
    /// Take the rebase-union of this constraint set with another
    pub fn rebase_union(
        &mut self,
        other: &Constraints,
        base: Option<&ValId>,
        separating: bool,
    ) -> bool {
        use Constraints::*;
        //TODO: report the need for an &mut *self to appease the borrow checker
        if let (Consistent(this), Consistent(other)) = (&mut *self, other) {
            if let Ok(change) = this.rebase_union(other, base, separating) {
                return change;
            }
        }
        let change = !matches!(&self, Contradiction);
        *self = Contradiction;
        change
    }
    /// Take the union of this constraint set with another
    #[inline]
    pub fn union(&mut self, other: &Constraints, separating: bool) -> bool {
        self.rebase_union(other, None, separating)
    }
    /// Require a constraint between two values
    pub fn require(
        &mut self,
        left: Option<ValId>,
        right: Option<ValId>,
        relationship: Relationship,
        separating: bool,
    ) -> bool {
        //TODO: handle infinities, and such...
        if let Ok(change) = self.try_require(left, right, relationship, separating) {
            return change;
        }
        let change = self.is_consistent();
        *self = Constraints::Contradiction;
        change
    }
    /// Require a rebased constraint on a value, returning an error on inconsistency
    pub fn rebase_require(
        &mut self,
        value: Option<ValId>,
        constraint: &Constraint,
        base: Option<&ValId>,
        separating: bool,
    ) -> bool {
        if let Ok(change) = self.try_rebase_require(value, constraint, base, separating) {
            return change;
        }
        let change = self.is_consistent();
        *self = Constraints::Contradiction;
        change
    }
    /// Try to iterate over this constraint set
    pub fn try_iter(&self) -> Result<impl Iterator<Item = (&Option<ValId>, &Constraint)>, Error> {
        match self {
            Constraints::Consistent(c) => Ok(c.iter()),
            _ => Err(Error::Contradiction),
        }
    }
    /// Iterate over this constraint set, returning the empty iterator in the case of an inconsistent constraint-set
    pub fn iter(&self) -> impl Iterator<Item = (&Option<ValId>, &Constraint)> {
        match self {
            Constraints::Consistent(c) => c.iter(),
            _ => NO_CONSTRAINTS_CONSISTENT.iter(),
        }
    }
    /// Get the constraint of a symbol, if any
    pub fn constraint(&self, symbol: &Option<ValId>) -> &Constraint {
        match self {
            Constraints::Consistent(c) => c.constraint(symbol),
            _ => &NO_CONSTRAINT,
        }
    }
    /// Get the values which come before or at the same time as this value, according to this constraint-set
    pub fn prev(&self, value: &Option<ValId>) -> impl Iterator<Item = (&ValId, Relationship)> {
        self.constraint(value)
            .iter()
            .filter_map(|(value, relationship)| match relationship.variance() {
                Covariant | Invariant => value.as_ref().map(|value| (value, *relationship)),
                _ => None,
            })
    }
    /// Get the strict dependencies of this value, according to this constraint set
    pub fn deps(&self, value: &Option<ValId>) -> impl Iterator<Item = (&ValId, Usage)> {
        self.constraint(value)
            .iter()
            .filter_map(|(value, relationship)| {
                match (relationship.variance(), relationship.strict()) {
                    (Covariant, true) => value.as_ref().map(|value| (value, relationship.usage())),
                    _ => None,
                }
            })
    }
    /// Check this constraint set for cycles
    pub fn cycle_check(&mut self, arg: &SymbolId, result: &ValId) -> bool {
        match self {
            Constraints::Consistent(consistent) => {
                if let Ok(change) = consistent.cycle_check(result, |(value, _)| {
                    if value.fv().contains(arg) {
                        Some(value)
                    } else {
                        None
                    }
                }) {
                    return change;
                }
            }
            Constraints::Contradiction => return false,
        };
        *self = Constraints::Contradiction;
        true
    }
    /// Get the code for this constraint set
    pub fn code(&self) -> u64 {
        match self {
            Constraints::Consistent(c) => c.code(),
            Constraints::Contradiction => 0xC0,
        }
    }
    /// Compute the free variable set of this constraint set
    pub fn fv(&self) -> SymbolSet {
        match self {
            Constraints::Consistent(c) => c.fv(),
            Constraints::Contradiction => SymbolSet::default(),
        }
    }
}

impl BitAnd<&'_ Constraints> for Constraints {
    type Output = Constraints;
    fn bitand(mut self, other: &'_ Constraints) -> Constraints {
        self.union(other, false);
        self
    }
}

impl BitAnd<Constraints> for &'_ Constraints {
    type Output = Constraints;
    fn bitand(self, mut other: Constraints) -> Constraints {
        other.union(self, false);
        other
    }
}

impl BitAnd<&'_ Constraints> for &'_ Constraints {
    type Output = Constraints;
    fn bitand(self, other: &Constraints) -> Constraints {
        self.clone() & other
    }
}

impl BitAnd<Constraints> for Constraints {
    type Output = Constraints;
    fn bitand(self, other: Constraints) -> Constraints {
        self & &other
    }
}

impl Mul<&'_ Constraints> for Constraints {
    type Output = Constraints;
    fn mul(mut self, other: &'_ Constraints) -> Constraints {
        self.union(other, true);
        self
    }
}

impl Mul<Constraints> for &'_ Constraints {
    type Output = Constraints;
    fn mul(self, mut other: Constraints) -> Constraints {
        other.union(self, true);
        other
    }
}

impl Mul<&'_ Constraints> for &'_ Constraints {
    type Output = Constraints;
    fn mul(self, other: &Constraints) -> Constraints {
        self.clone() * other
    }
}

impl Mul<Constraints> for Constraints {
    type Output = Constraints;
    fn mul(self, other: Constraints) -> Constraints {
        self * &other
    }
}

impl Default for Constraints {
    #[inline]
    fn default() -> Constraints {
        NO_CONSTRAINTS
    }
}

impl Debug for Constraints {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        match self {
            Constraints::Consistent(c) => Debug::fmt(c, fmt),
            Constraints::Contradiction => write!(fmt, "{{!}}"),
        }
    }
}
