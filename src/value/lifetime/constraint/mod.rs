use super::*;

mod consistent;
mod set;
pub use consistent::*;
pub use set::*;

/// The trivial constraint
pub const NO_CONSTRAINT: Constraint = Constraint {
    relationships: None,
    usage: Usage::UNUSED,
};

/// A constraint on an instant
#[derive(Clone, Eq)]
pub struct Constraint {
    /// The relationships this constraint has with another, if any
    relationships: Option<HashMap<Option<ValId>, Relationship>>,
    /// The usage of this value by it's relations, or `None` in the case of an inconsistency
    usage: Usage,
}

impl Default for Constraint {
    #[inline]
    fn default() -> Constraint {
        NO_CONSTRAINT
    }
}

impl Constraint {
    /// Create a new, empty constraint-set
    #[inline]
    pub fn new() -> Constraint {
        Constraint::default()
    }
    /// Get the usage of the value thus constrained
    pub fn usage(&self) -> Usage {
        self.usage
    }
    /// Check whether this constraint set is empty
    pub fn is_empty(&self) -> bool {
        let is_empty = self
            .relationships
            .as_ref()
            .map(HashMap::is_empty)
            .unwrap_or(true);
        if is_empty {
            debug_assert_eq!(self.usage, Usage::UNUSED);
        }
        is_empty
    }
    /// Get the relationships of this constraint-set
    fn get_relationships(&mut self) -> &mut HashMap<Option<ValId>, Relationship> {
        self.relationships.get_or_insert_with(HashMap::default)
    }
    /// Rebase-union this constraint with another
    ///
    /// On failure, leaves this constraint in an unspecified but valid state
    pub fn rebase_union(
        &mut self,
        other: &Constraint,
        base: Option<&ValId>,
        separating: bool,
    ) -> Result<bool, Error> {
        let mut change = false;
        for (value, relationship) in other.iter() {
            let target = value.as_ref().or(base).cloned();
            change |= self.require(target, *relationship, separating)?;
        }
        Ok(change)
    }
    /// Union this constraint with another
    ///
    /// On failure, leaves this constraint in an unspecified but valid state
    #[inline]
    pub fn union(&mut self, other: &Constraint, separating: bool) -> Result<bool, Error> {
        self.rebase_union(other, None, separating)
    }
    /// Require a constraint from this set, but with a loop check
    pub fn loop_require(
        &mut self,
        instant: Option<ValId>,
        relationship: Relationship,
        separating: bool,
    ) -> Result<bool, Error> {
        if instant.is_none() {
            if relationship.strict() {
                Err(Error::Cycle)
            } else {
                Ok(false)
            }
        } else {
            self.require(instant, relationship, separating)
        }
    }
    /// Require a constraint from this set, returning an error if it is incompatible. Returns `true` if a change was made.
    pub fn require(
        &mut self,
        instant: Option<ValId>,
        relationship: Relationship,
        separating: bool,
    ) -> Result<bool, Error> {
        if relationship.is_contradiction() {
            return Err(Error::Contradiction);
        }
        if instant.as_ref().map(ValId::is_const).unwrap_or_default() {
            return Ok(false);
        }
        let usage = self.usage.sep_conj(relationship.dependent_usage())?;
        let relationships = self.get_relationships();
        let mut o = match relationships.entry(instant) {
            Entry::Vacant(v) => {
                v.insert(relationship);
                self.usage = usage;
                return Ok(true);
            }
            Entry::Occupied(o) => o,
        };
        let curr = o.get_mut();
        let meet = if separating {
            curr.sep_conj(relationship)
        } else {
            curr.meet(relationship)
        };
        if meet.is_contradiction() {
            return Err(Error::IncompatibleRelationships(relationship, *curr));
        }
        let change = *curr != meet;
        *curr = meet;
        self.usage = usage;
        Ok(change)
    }
    /// Get the constrained relationship with a value, if any
    pub fn relationship(&self, instant: &Option<ValId>) -> Option<Relationship> {
        self.relationships
            .as_ref()
            .map(|relationships| relationships.get(instant))
            .flatten()
            .copied()
    }
    /// Compute the free variable set of this constraint-set
    pub fn fv(&self) -> SymbolSet {
        let mut set = SymbolSet::default();
        for value in self.keys().flatten() {
            set.insert_set(value.fv());
        }
        set
    }
    /// Iterate over this constraint's components, or yield falsum
    pub fn iter(&self) -> impl Iterator<Item = (&Option<ValId>, &Relationship)> {
        self.relationships.iter().flat_map(HashMap::iter)
    }
    /// Get the keys of this constraint
    pub fn keys(&self) -> impl Iterator<Item = &Option<ValId>> {
        self.relationships.iter().flat_map(HashMap::keys)
    }
    /// Get the code array of this constraint
    pub fn code_array(&self) -> SmallVec<[u64; 10]> {
        let mut key_codes: SmallVec<[u64; 10]> = self
            .iter()
            .map(|(value, rel)| value.as_ref().map(ValId::code).unwrap_or_default() ^ rel.code())
            .collect();
        key_codes.sort_unstable();
        key_codes
    }
    /// Get the default hasher for constraints    
    pub fn get_hasher() -> AHasher {
        AHasher::new_with_keys(34, 2123)
    }
    /// Get the code of this constraint
    pub fn code(&self) -> u64 {
        let mut hasher = Self::get_hasher();
        self.hash(&mut hasher);
        hasher.finish()
    }
}

impl Hash for Constraint {
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.usage.hash(hasher);
        self.code_array().hash(hasher);
    }
}

impl PartialEq for Constraint {
    fn eq(&self, other: &Constraint) -> bool {
        self.usage == other.usage
            && (self.is_empty() && other.is_empty() || (self.relationships == other.relationships))
    }
}

impl Debug for Constraint {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{:?}", self.usage)?;
        if let Some(relationships) = &self.relationships {
            let mut printer = fmt.debug_map();
            for (value, relationship) in relationships.iter() {
                if let Some(value) = value {
                    printer.entry(value, relationship);
                } else {
                    printer.entry(&"self", relationship);
                }
            }
            return printer.finish();
        }
        write!(fmt, "{{}}")
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use Constraints::Contradiction;
    #[test]
    fn default() {
        assert_eq!(Constraint::default(), NO_CONSTRAINT);
    }
    #[test]
    #[allow(clippy::eq_op)]
    fn basic_union_properties() {
        // Trivials
        assert_eq!(NO_CONSTRAINTS & NO_CONSTRAINTS, NO_CONSTRAINTS);
        assert_eq!(NO_CONSTRAINTS & Contradiction, Contradiction);
        assert_eq!(Contradiction & NO_CONSTRAINTS, Contradiction);
        assert_ne!(NO_CONSTRAINTS, Contradiction);
        // x, y
        let x = Some(SymbolId::param(FREE_LIFETIME.clone()).unwrap().into_valid());
        let y = Some(SymbolId::param(FREE_LIFETIME.clone()).unwrap().into_valid());

        // x <= y
        let mut x_le_y = Constraints::default();
        x_le_y.require(x.clone(), y.clone(), Relationship::LE, true);
        assert_eq!(x_le_y, x_le_y);
        assert_ne!(x_le_y, NO_CONSTRAINTS);
        assert_ne!(x_le_y, Contradiction);
        let mut le_y = Constraint::default();
        le_y.require(y.clone(), Relationship::LE, true).unwrap();
        assert_eq!(le_y, le_y);
        let mut ge_x = Constraint::default();
        ge_x.require(x.clone(), Relationship::GE, true).unwrap();
        assert_eq!(ge_x, ge_x);
        assert_ne!(le_y, ge_x);
        assert_eq!(*x_le_y.constraint(&x), le_y);
        assert_eq!(*x_le_y.constraint(&y), ge_x);
        assert_eq!(x_le_y, &x_le_y & &x_le_y);
        assert_eq!(x_le_y, &x_le_y & NO_CONSTRAINTS);
        assert_eq!(x_le_y, NO_CONSTRAINTS & &x_le_y);
        assert_eq!(x_le_y, &NO_CONSTRAINTS & &x_le_y);
        assert_eq!(x_le_y, &x_le_y & NO_CONSTRAINTS);
        assert_eq!(x_le_y, NO_CONSTRAINTS & &x_le_y);
        assert_eq!(x_le_y, &NO_CONSTRAINTS & &x_le_y);
        assert_eq!(Contradiction, &x_le_y & Contradiction);
        assert_eq!(Contradiction, Contradiction & &x_le_y);
        assert_eq!(Contradiction, &Contradiction & &x_le_y);
        assert_eq!(Contradiction, &x_le_y & Contradiction);
        assert_eq!(Contradiction, Contradiction & &x_le_y);
        assert_eq!(Contradiction, &Contradiction & &x_le_y);

        // x >= y
        let mut x_ge_y = Constraints::default();
        x_ge_y.require(x.clone(), y.clone(), Relationship::GE, true);
        assert_eq!(x_ge_y, x_ge_y);
        assert_ne!(x_ge_y, x_le_y);
        assert_ne!(x_ge_y, NO_CONSTRAINTS);
        assert_ne!(x_ge_y, Contradiction);
        let mut ge_y = Constraint::default();
        ge_y.require(y.clone(), Relationship::GE, true).unwrap();
        assert_eq!(ge_y, ge_y);
        assert_ne!(ge_y, le_y);
        assert_ne!(ge_y, ge_x);
        let mut le_x = Constraint::default();
        le_x.require(x.clone(), Relationship::LE, true).unwrap();
        assert_eq!(le_x, le_x);
        assert_ne!(le_x, le_y);
        assert_eq!(*x_ge_y.constraint(&x), ge_y);
        assert_eq!(*x_ge_y.constraint(&y), le_x);
        assert_eq!(x_ge_y, &x_ge_y & &x_ge_y);
        assert_eq!(x_ge_y, &x_ge_y & NO_CONSTRAINTS);
        assert_eq!(x_ge_y, NO_CONSTRAINTS & &x_ge_y);
        assert_eq!(x_ge_y, &NO_CONSTRAINTS & &x_ge_y);
        assert_eq!(x_ge_y, &x_ge_y & NO_CONSTRAINTS);
        assert_eq!(x_ge_y, NO_CONSTRAINTS & &x_ge_y);
        assert_eq!(x_ge_y, &NO_CONSTRAINTS & &x_ge_y);
        assert_eq!(Contradiction, &x_le_y & Contradiction);
        assert_eq!(Contradiction, Contradiction & &x_ge_y);
        assert_eq!(Contradiction, &Contradiction & &x_ge_y);
        assert_eq!(Contradiction, &x_ge_y & Contradiction);
        assert_eq!(Contradiction, Contradiction & &x_ge_y);
        assert_eq!(Contradiction, &Contradiction & &x_ge_y);

        // x == y
        let mut x_eq_y = Constraints::default();
        x_eq_y.require(x.clone(), y.clone(), Relationship::EQ, true);
        assert_eq!(x_eq_y, x_eq_y);
        assert_ne!(x_eq_y, x_le_y);
        assert_ne!(x_eq_y, x_ge_y);
        assert_ne!(x_eq_y, NO_CONSTRAINTS);
        assert_ne!(x_eq_y, Contradiction);
        let mut eq_y = Constraint::default();
        eq_y.require(y.clone(), Relationship::EQ, true).unwrap();
        assert_eq!(eq_y, eq_y);
        let mut eq_x = Constraint::default();
        eq_x.require(x.clone(), Relationship::EQ, true).unwrap();
        assert_eq!(eq_x, eq_x);
        assert_ne!(eq_x, eq_y);
        assert_eq!(*x_eq_y.constraint(&x), eq_y);
        assert_eq!(*x_eq_y.constraint(&y), eq_x);
        assert_eq!(x_eq_y, &x_eq_y & NO_CONSTRAINTS);
        assert_eq!(x_eq_y, NO_CONSTRAINTS & &x_eq_y);
        assert_eq!(x_eq_y, &NO_CONSTRAINTS & &x_eq_y);
        assert_eq!(x_eq_y, &x_eq_y & NO_CONSTRAINTS);
        assert_eq!(x_eq_y, NO_CONSTRAINTS & &x_eq_y);
        assert_eq!(x_eq_y, &NO_CONSTRAINTS & &x_eq_y);
        assert_eq!(Contradiction, &x_le_y & Contradiction);
        assert_eq!(Contradiction, Contradiction & &x_eq_y);
        assert_eq!(Contradiction, &Contradiction & &x_eq_y);
        assert_eq!(Contradiction, &x_eq_y & Contradiction);
        assert_eq!(Contradiction, Contradiction & &x_eq_y);
        assert_eq!(Contradiction, &Contradiction & &x_eq_y);

        // (>=) /\ (<=) <=> (==)
        assert_eq!(x_eq_y, &x_ge_y & &x_le_y);
        assert_eq!(x_eq_y, &x_le_y & &x_ge_y);
        assert_eq!(x_eq_y, &x_ge_y & &x_eq_y);
        assert_eq!(x_eq_y, &x_le_y & &x_eq_y);

        // x != y
        let mut x_ne_y = Constraints::default();
        x_ne_y.require(x.clone(), y.clone(), Relationship::NE, true);
        assert_eq!(x_ne_y, x_ne_y);
        assert_ne!(x_ne_y, x_le_y);
        assert_ne!(x_ne_y, x_ge_y);
        assert_ne!(x_ne_y, x_eq_y);
        assert_ne!(x_ne_y, NO_CONSTRAINTS);
        assert_ne!(x_ne_y, Contradiction);
        let mut ne_y = Constraint::default();
        ne_y.require(y.clone(), Relationship::NE, true).unwrap();
        assert_eq!(ne_y, ne_y);
        let mut ne_x = Constraint::default();
        ne_x.require(x.clone(), Relationship::NE, true).unwrap();
        assert_eq!(ne_x, ne_x);
        assert_ne!(ne_x, ne_y);
        assert_eq!(*x_ne_y.constraint(&x), ne_y);
        assert_eq!(*x_ne_y.constraint(&y), ne_x);
        assert_eq!(x_ne_y, &x_ne_y & NO_CONSTRAINTS);
        assert_eq!(x_ne_y, NO_CONSTRAINTS & &x_ne_y);
        assert_eq!(x_ne_y, &NO_CONSTRAINTS & &x_ne_y);
        assert_eq!(x_ne_y, &x_ne_y & NO_CONSTRAINTS);
        assert_eq!(x_ne_y, NO_CONSTRAINTS & &x_ne_y);
        assert_eq!(x_ne_y, &NO_CONSTRAINTS & &x_ne_y);
        assert_eq!(Contradiction, &x_ne_y & Contradiction);
        assert_eq!(Contradiction, Contradiction & &x_ne_y);
        assert_eq!(Contradiction, &Contradiction & &x_ne_y);
        assert_eq!(Contradiction, &x_ne_y & Contradiction);
        assert_eq!(Contradiction, Contradiction & &x_ne_y);
        assert_eq!(Contradiction, &Contradiction & &x_ne_y);

        // (==) /\ (!=) ==> 0
        assert_eq!(Contradiction, &x_eq_y & &x_ne_y);
        assert_eq!(Contradiction, &x_ne_y & &x_eq_y);
    }
}
