/*!
A dependency between two values
*/
use crate::value::*;

/// Dependency metadata for a symbol
#[derive(Copy, Clone, Eq, PartialEq, Hash)]
pub struct Dependency {
    /// The variance of this value with respect to a free variable's value
    pub variance: Variance,
    /// The relationship between this value and a symbol
    pub relationship: Relationship,
}

impl PartialOrd for Dependency {
    #[inline]
    fn partial_cmp(&self, other: &Dependency) -> Option<Ordering> {
        self.const_cmp(*other)
    }
}

impl Debug for Dependency {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        if *self == Dependency::CONTRADICTION {
            write!(fmt, "!")
        } else {
            write!(fmt, "SymDep({:?}, {:?})", self.variance, self.relationship)
        }
    }
}

impl Dependency {
    /// A fully consumed dependency
    pub const USED: Dependency = Dependency::covar(Usage::USED);
    /// A contradictory dependency
    pub const CONTRADICTION: Dependency = Dependency {
        variance: Invariant,
        relationship: Relationship::CN,
    };
    /// A null dependency
    pub const NULL: Dependency = Dependency {
        variance: Bivariant,
        relationship: Relationship::TV,
    };
    /// Get the covariant symbol dependency of a given usage
    #[inline]
    pub const fn covar(usage: Usage) -> Dependency {
        Dependency {
            variance: Covariant,
            relationship: Relationship::dependency(usage),
        }
    }
    /// Constant-compare two dependencies
    #[inline]
    pub const fn const_cmp(self, other: Dependency) -> Option<Ordering> {
        lattice_ord_opt(
            other.variance.const_cmp(self.variance),
            self.relationship.const_cmp(other.relationship),
        )
    }
    /// Flip a dependency
    #[inline]
    pub fn flip(self) -> Dependency {
        Dependency {
            variance: self.variance.flip(),
            relationship: self.relationship.flip(),
        }
    }
    /// Take a given dependency with respect to a given symbol
    #[inline]
    pub fn of(self, symbol: &SymbolId) -> Dependency {
        Dependency {
            variance: self.variance.of(symbol),
            relationship: self.relationship.of(symbol),
        }
    }
    /// Take the join of two dependency kinds
    #[inline]
    pub fn join(self, other: Dependency) -> Dependency {
        Dependency {
            variance: self.variance.join(other.variance),
            relationship: self.relationship.join(other.relationship),
        }
    }
    /// Take the meet of two dependency kinds
    #[inline]
    pub fn meet(self, other: Dependency) -> Dependency {
        Dependency {
            variance: self.variance.meet(other.variance),
            relationship: self.relationship.meet(other.relationship),
        }
    }
    /// Iterate over all possible symbol dependencies
    #[inline]
    pub fn iter_all() -> impl Iterator<Item = Dependency> {
        RELATIONSHIPS
            .iter()
            .copied()
            .map(|relationship| {
                VARIANCES.iter().copied().map(move |variance| Dependency {
                    variance,
                    relationship,
                })
            })
            .flatten()
    }
}
