/*!
`isotope` instants
*/

use super::*;

/// The instant at infinity
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Infty;

impl Value for Infty {
    #[inline]
    fn ty(&self) -> &ValId {
        &*FREE_LIFETIME
    }
    #[inline]
    fn is_instant(&self) -> bool {
        true
    }
    #[inline]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Infty(Infty)
    }
    #[inline]
    fn into_valid(self) -> ValId {
        INFTY.clone()
    }
    #[inline]
    fn code(&self) -> u64 {
        0x494e465459
    }
}

/// The instant at zero, i.e. initialization
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Zero;

impl Value for Zero {
    #[inline]
    fn ty(&self) -> &ValId {
        &*FREE_LIFETIME
    }
    #[inline]
    fn is_instant(&self) -> bool {
        true
    }
    #[inline]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Zero(Zero)
    }
    #[inline]
    fn into_valid(self) -> ValId {
        ZERO.clone()
    }
    #[inline]
    fn code(&self) -> u64 {
        0x5a45524f
    }
}

lazy_static! {
    /// The instant at infinity
    pub static ref INFTY: ValId = ValId::new_direct(ValueEnum::Infty(Infty));
    /// The instant at zero
    pub static ref ZERO: ValId = ValId::new_direct(ValueEnum::Zero(Zero));
}
