/*!
Linear and substructural typing

TODO: go back to linearity only based model, tag symbols
*/
use super::*;

/// Usage of a value
#[derive(Copy, Clone, Eq, PartialEq, Hash, Default)]
pub struct Usage {
    /// Whether this value has been *consumed*, and hence can no longer be used in an affine term
    pub consumed: bool,
    /// Whether this value has *not* been observed, and hence must be observed
    pub unobserved: bool,
}

impl PartialOrd for Usage {
    #[inline]
    fn partial_cmp(&self, other: &Usage) -> Option<Ordering> {
        self.const_cmp(*other)
    }
}

impl Debug for Usage {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        Display::fmt(self.into_str(), fmt)
    }
}

impl Usage {
    /// A usage which has been consumed, without being observed
    pub const CONSUMED: Usage = Usage {
        consumed: true,
        unobserved: true,
    };
    /// A value which has neither been consumed or observed
    pub const UNUSED: Usage = Usage {
        consumed: false,
        unobserved: true,
    };
    /// A value which has been consumed and observed
    pub const USED: Usage = Usage {
        consumed: true,
        unobserved: false,
    };
    /// A value which has been observed, but not consumed
    pub const OBSERVED: Usage = Usage {
        consumed: false,
        unobserved: false,
    };

    /// A list of all possible usages
    pub const USAGES: [Usage; 4] = [Self::CONSUMED, Self::UNUSED, Self::USED, Self::OBSERVED];

    /// Get a string representation of this usage
    #[inline]
    pub const fn into_str(self) -> &'static str {
        match self {
            Usage::CONSUMED => "Consumed",
            Usage::UNUSED => "Unused",
            Usage::USED => "Used",
            Usage::OBSERVED => "Observed",
        }
    }

    /// Check whether two usages are equal in a constant way
    #[inline]
    pub const fn const_eq(self, other: Usage) -> bool {
        self.consumed == other.consumed && self.unobserved == other.unobserved
    }

    /// Take the meet of two usages
    #[inline]
    pub const fn meet(self, other: Usage) -> Usage {
        Usage {
            consumed: self.consumed && other.consumed,
            unobserved: self.unobserved && other.unobserved,
        }
    }

    /// Take the join of two usages
    #[inline]
    pub const fn join(self, other: Usage) -> Usage {
        Usage {
            consumed: self.consumed || other.consumed,
            unobserved: self.unobserved || other.unobserved,
        }
    }

    /// Take the conjunction of two usages
    #[inline]
    pub const fn conj(self, other: Usage) -> Usage {
        Usage {
            consumed: self.consumed || other.consumed,
            unobserved: self.unobserved && other.unobserved,
        }
    }

    /// Take the separating conjunction of two usages
    #[inline]
    pub const fn sep_conj(self, other: Usage) -> Result<Usage, Error> {
        if self.consumed && other.consumed {
            Err(Error::DoubleUse(self, other))
        } else {
            Ok(self.conj(other))
        }
    }
    /// Compare two usages in a constant way
    #[inline]
    pub const fn const_cmp(self, other: Usage) -> Option<Ordering> {
        let consumed_cmp = bool_cmp(self.consumed, other.consumed);
        let unobserved_cmp = bool_cmp(self.unobserved, other.unobserved);
        lattice_ord(consumed_cmp, unobserved_cmp)
    }
    /// Get the minimal linearity for a given usage
    #[inline]
    pub const fn min_linearity(self) -> Linearity {
        Linearity {
            affine: self.consumed,
            relevant: self.unobserved,
        }
    }
    /// Get a code for this linearity
    #[inline]
    pub const fn code(self) -> u64 {
        ((self.consumed as u64) << 2) | ((self.unobserved as u64) << 3)
    }
}

impl BitAnd for Usage {
    type Output = Usage;
    #[inline]
    fn bitand(self, other: Usage) -> Usage {
        self.meet(other)
    }
}

impl BitOr for Usage {
    type Output = Usage;
    #[inline]
    fn bitor(self, other: Usage) -> Usage {
        self.join(other)
    }
}

impl BitAndAssign for Usage {
    fn bitand_assign(&mut self, other: Usage) {
        *self = *self & other
    }
}

impl BitOrAssign for Usage {
    fn bitor_assign(&mut self, other: Usage) {
        *self = *self | other
    }
}

impl Mul for Usage {
    type Output = Result<Usage, Error>;
    #[inline]
    fn mul(self, other: Usage) -> Result<Usage, Error> {
        self.sep_conj(other)
    }
}

/// Substructurality requirements on a value
#[derive(Copy, Clone, Eq, PartialEq, Hash, Default)]
pub struct Linearity {
    /// Whether this value is *affine*, i.e. can be used at most once in a concrete term
    pub affine: bool,
    /// Whether this value is *relevant*, i.e. must be used at least once in a concrete term
    pub relevant: bool,
}

impl PartialOrd for Linearity {
    #[inline]
    fn partial_cmp(&self, other: &Linearity) -> Option<Ordering> {
        self.const_cmp(*other)
    }
}

impl Debug for Linearity {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        Display::fmt(self.into_str(), fmt)
    }
}

impl Linearity {
    /// A linear value, which must be used exactly once in a concrete term containing it
    pub const LINEAR: Linearity = Linearity {
        affine: true,
        relevant: true,
    };
    /// A relevant value, which must be used at least once in a concrete term producing it
    pub const RELEVANT: Linearity = Linearity {
        affine: false,
        relevant: true,
    };
    /// An affine value, which may only be used once in a concrete term
    pub const AFFINE: Linearity = Linearity {
        affine: true,
        relevant: false,
    };
    /// An unrestricted, or "nonlinear," value
    pub const NONLINEAR: Linearity = Linearity {
        affine: false,
        relevant: false,
    };

    /// A list of all possible linearities
    pub const LINEARITIES: [Linearity; 4] =
        [Self::LINEAR, Self::RELEVANT, Self::AFFINE, Self::NONLINEAR];
    /// Check whether a linearity is nonlinear
    #[inline]
    pub const fn is_nonlinear(self) -> bool {
        !(self.affine || self.relevant)
    }
    /// Check whether two linearities are equal in a constant way
    #[inline]
    pub const fn const_eq(self, other: Linearity) -> bool {
        self.affine == other.affine && self.relevant == other.relevant
    }
    /// Compare two linearities in a constant way
    #[inline]
    pub const fn const_cmp(self, other: Linearity) -> Option<Ordering> {
        let affine_cmp = bool_cmp(self.affine, other.affine);
        let relevant_cmp = bool_cmp(self.relevant, other.relevant);
        lattice_ord(affine_cmp, relevant_cmp)
    }
    /// Take the meet of two linearities
    #[inline]
    pub const fn meet(self, other: Linearity) -> Linearity {
        Linearity {
            affine: self.affine && other.affine,
            relevant: self.relevant && other.relevant,
        }
    }
    /// Take the join of two linearities
    #[inline]
    pub const fn join(self, other: Linearity) -> Linearity {
        Linearity {
            affine: self.affine || other.affine,
            relevant: self.relevant || other.relevant,
        }
    }
    /// Get a string representation of this linearity
    #[inline]
    pub const fn into_str(self) -> &'static str {
        match self {
            Linearity::NONLINEAR => "Nonlinear",
            Linearity::AFFINE => "Affine",
            Linearity::RELEVANT => "Relevant",
            Linearity::LINEAR => "Linear",
        }
    }
    /// Get the maximum possible usage for a linearity
    #[inline]
    pub const fn max_usage(self) -> Usage {
        Usage {
            consumed: self.affine,
            unobserved: self.relevant,
        }
    }
}

impl BitAnd for Linearity {
    type Output = Linearity;
    #[inline]
    fn bitand(self, other: Linearity) -> Linearity {
        self.meet(other)
    }
}

impl BitOr for Linearity {
    type Output = Linearity;
    #[inline]
    fn bitor(self, other: Linearity) -> Linearity {
        self.join(other)
    }
}

impl BitAndAssign for Linearity {
    fn bitand_assign(&mut self, other: Linearity) {
        *self = *self & other
    }
}

impl BitOrAssign for Linearity {
    fn bitor_assign(&mut self, other: Linearity) {
        *self = *self | other
    }
}

#[cfg(test)]
mod test {
    use super::*;
    #[test]
    fn max_usage_min_linearity() {
        for &usage in &Usage::USAGES {
            assert_eq!(usage.min_linearity().max_usage(), usage)
        }
        for &linearity in &Linearity::LINEARITIES {
            assert_eq!(linearity.max_usage().min_linearity(), linearity)
        }
    }
    #[test]
    fn usage_meet_join_conj_sep() {
        let o = Usage::OBSERVED;
        let u = Usage::USED;
        let n = Usage::UNUSED;
        let c = Usage::CONSUMED;
        let tuples = [
            (o, o, o, o, o, Some(o)),
            (o, u, o, u, u, Some(u)),
            (o, n, o, n, o, Some(o)),
            (o, c, o, c, u, Some(u)),
            (u, u, u, u, u, None),
            (u, n, o, c, u, Some(u)),
            (u, c, u, c, u, None),
            (n, n, n, n, n, Some(n)),
            (n, c, n, c, c, Some(c)),
            (c, c, c, c, c, None),
        ];
        for &(left, right, meet, join, conj, sep) in tuples.iter() {
            assert_eq!(left & right, meet, "{:?} & {:?} == {:?}", left, right, meet);
            assert_eq!(left | right, join, "{:?} | {:?} == {:?}", left, right, join);
            assert_eq!(
                left.conj(right),
                conj,
                "{:?}.conj({:?}) == {:?}",
                left,
                right,
                conj
            );
            assert_eq!(right & left, meet, "{:?} & {:?} == {:?}", right, left, meet);
            assert_eq!(right | left, join, "{:?} | {:?} == {:?}", right, left, join);
            assert_eq!(
                right.conj(left),
                conj,
                "{:?}.conj({:?}) == {:?}",
                right,
                left,
                conj
            );
            let lrsep = sep.ok_or(Error::DoubleUse(left, right));
            let rlsep = sep.ok_or(Error::DoubleUse(right, left));
            assert_eq!(
                left * right,
                lrsep,
                "{:?} * {:?} == {:?}",
                left,
                right,
                lrsep
            );
            assert_eq!(
                right * left,
                rlsep,
                "{:?} * {:?} == {:?}",
                right,
                left,
                rlsep
            );
        }
    }
    #[test]
    fn usage_partial_ord() {
        use Ordering::*;
        let o = Usage::OBSERVED;
        let u = Usage::USED;
        let n = Usage::UNUSED;
        let c = Usage::CONSUMED;
        let ords = [
            (o, o, Some(Equal)),
            (o, u, Some(Less)),
            (o, n, Some(Less)),
            (o, c, Some(Less)),
            (u, u, Some(Equal)),
            (u, n, None),
            (u, c, Some(Less)),
            (n, n, Some(Equal)),
            (n, c, Some(Less)),
            (c, c, Some(Equal)),
        ];
        for (left, right, ord) in ords.iter() {
            assert_eq!(
                left.partial_cmp(right),
                *ord,
                "{:?}.partial_cmp({:?})",
                left,
                right
            );
            assert_eq!(
                right.partial_cmp(left),
                ord.map(Ordering::reverse),
                "{:?}.partial_cmp({:?})",
                right,
                left
            );
        }
        for &left in &Usage::USAGES {
            for &right in &Usage::USAGES {
                assert!(left.meet(right) <= left);
                assert!(left.join(right) >= left);
            }
        }
    }
    #[test]
    fn linearity_meet_join() {
        let n = Linearity::NONLINEAR;
        let a = Linearity::AFFINE;
        let r = Linearity::RELEVANT;
        let l = Linearity::LINEAR;
        let tuples = [
            (n, n, n, n),
            (n, a, n, a),
            (n, r, n, r),
            (n, l, n, l),
            (a, a, a, a),
            (a, r, n, l),
            (a, l, a, l),
            (r, r, r, r),
            (r, l, r, l),
            (l, l, l, l),
        ];
        for &(left, right, meet, join) in tuples.iter() {
            assert_eq!(left & right, meet, "{:?} & {:?} == {:?}", left, right, meet);
            assert_eq!(right & left, meet, "{:?} & {:?} == {:?}", right, left, meet);
            assert_eq!(left | right, join, "{:?} | {:?} == {:?}", left, right, join);
            assert_eq!(right | left, join, "{:?} | {:?} == {:?}", right, left, join);
        }
    }
    #[test]
    fn linearity_partial_ord() {
        use Ordering::*;
        let n = Linearity::NONLINEAR;
        let a = Linearity::AFFINE;
        let r = Linearity::RELEVANT;
        let l = Linearity::LINEAR;
        let ords = [
            (n, n, Some(Equal)),
            (n, a, Some(Less)),
            (n, r, Some(Less)),
            (n, l, Some(Less)),
            (a, a, Some(Equal)),
            (a, r, None),
            (a, l, Some(Less)),
            (r, r, Some(Equal)),
            (r, l, Some(Less)),
            (l, l, Some(Equal)),
        ];
        for (left, right, ord) in ords.iter() {
            assert_eq!(
                left.partial_cmp(right),
                *ord,
                "{:?}.partial_cmp({:?})",
                left,
                right
            );
            assert_eq!(
                right.partial_cmp(left),
                ord.map(Ordering::reverse),
                "{:?}.partial_cmp({:?})",
                right,
                left
            );
        }
        for &left in &Linearity::LINEARITIES {
            for &right in &Linearity::LINEARITIES {
                assert!(left.meet(right) <= left);
                assert!(left.join(right) >= left);
            }
        }
    }
}
