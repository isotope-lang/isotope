/*!
`isotope` lifetimes and instants
*/
use super::*;

mod constraint;
mod dependency;
mod instant;
mod linear;
mod matches;
mod relationship;
pub use constraint::*;
pub use dependency::*;
pub use instant::*;
pub use linear::*;
pub use matches::*;
pub use relationship::*;

/// The type of instants which satisfy a given constraint
#[derive(Clone, Eq, PartialEq)]
pub struct Lifetime {
    /// The constraint instants in this lifetime are under
    constraint: Constraint,
    /// The free variable set of this lifetime
    fv: SymbolSet,
    /// The code of this instant
    code: u64,
}

impl Lifetime {
    /// Create a new lifetime from a constraint
    pub fn new(constraint: Constraint) -> Lifetime {
        let fv = constraint.fv();
        let mut tmp = Lifetime {
            constraint,
            fv,
            code: 0,
        };
        tmp.fix_code();
        tmp
    }
    /// The unconstrained lifetime
    pub fn free() -> Lifetime {
        Self::new(Constraint::default())
    }
    /// Get the hasher used for lifetimes
    pub fn get_hasher() -> AHasher {
        AHasher::new_with_keys(34, 2123)
    }
    /// Fix the code of a constraint. Should be a no-op for user-facing code
    fn fix_code(&mut self) {
        let mut hasher = Self::get_hasher();
        self.constraint.hash(&mut hasher);
        self.code = hasher.finish()
    }
}

impl Debug for Lifetime {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "Lifetime{:#?}", self.constraint)
    }
}

#[allow(clippy::derive_hash_xor_eq)]
impl Hash for Lifetime {
    #[inline]
    fn hash<H: Hasher>(&self, hasher: &mut H) {
        self.code().hash(hasher)
    }
}

impl Value for Lifetime {
    #[inline]
    fn is_ty(&self) -> bool {
        true
    }
    #[inline]
    fn is_groupoid(&self) -> bool {
        false
    }
    #[inline]
    fn ty(&self) -> &ValId {
        &*SET
    }
    #[inline]
    fn fv(&self) -> &SymbolSet {
        &self.fv
    }
    #[inline]
    fn is_lifetime(&self) -> bool {
        true
    }
    #[inline]
    fn into_enum(self) -> ValueEnum {
        ValueEnum::Lifetime(self)
    }
    #[inline]
    fn into_valid(self) -> ValId {
        //TODO: free instant...
        ValId::new_direct(ValueEnum::Lifetime(self))
    }
    #[inline]
    fn code(&self) -> u64 {
        self.code
    }
    fn contains(&self, value: &ValId) -> Result<Match, Error> {
        let _constraint = value.instant_constraint()?;
        unimplemented!()
    }
}

lazy_static! {
    /// The unconstrainted lifetime
    pub static ref FREE_LIFETIME: ValId = ValId::new_direct(ValueEnum::Lifetime(Lifetime::free()));
}
