/*!
`isotope` type matching
*/

use super::*;

/// A match between two types
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Match {
    /// The minimal relationship between the matched value and the resulting function application
    pub relationship: Relationship,
    /// The (additional) constraints required for this match to be concrete
    pub constraints: Constraints,
}

impl Match {
    /// Construct a match for a simple dependency, having a given usage
    #[inline]
    pub fn dependency(usage: Usage) -> Match {
        Match {
            relationship: Relationship::dependency(usage),
            constraints: Constraints::default(),
        }
    }
    /// Clip a match for a value's linearity
    #[inline]
    pub fn clip(self, linearity: Linearity) -> Match {
        Match {
            relationship: self.relationship.meet_usage(linearity.max_usage()),
            ..self
        }
    }
    /// Convert a match to a constraint set with a base and value
    #[inline]
    pub fn into_constraints(mut self, base: Option<ValId>, value: Option<ValId>) -> Constraints {
        self.constraints
            .require(base, value, self.relationship, true);
        self.constraints
    }
}

impl Default for Match {
    fn default() -> Match {
        Match {
            relationship: Relationship::dependency(Usage::OBSERVED),
            constraints: Constraints::default(),
        }
    }
}

/// An inconsistent match
pub const INCONSISTENT_MATCH: Match = Match {
    relationship: Relationship::CN,
    constraints: Constraints::Contradiction,
};
