/*!
Function types and variables
*/
use super::*;

/// A parameter
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Param {
    /// The name of this parameter, if any
    pub name: Option<String>,
    /// The type of this parameter, if any
    pub ty: Option<Arc<Expr>>,
}

/// A parametrized value
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Parametrized {
    /// The parameter of this value
    pub param: Param,
    /// The dependency of this parametrization
    pub dep: Dependency,
    /// The value being parametrized
    pub value: Arc<Expr>,
}

/// A function kind
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum FnKind {
    /// This function can be used and copied freely
    FnCopy,
    /// This function can be used freely, but not copied
    Fn,
    /// This function can be used once
    FnOnce,
    /// This function type cannot be discarded, but can be copied
    ExCopy,
    /// This function type must be used once,
    Fx,
    /// This function type cannot be discared or copied
    Ex,
    /// This function type cannot be discarded, and must be used once
    ExOnce,
    /// This function type must be used exactly once
    FxOnce,
}

impl FnKind {
    /// Get the functional linearity associated with this function kind given a dependency
    #[inline]
    pub fn fun_lin(self, dep: Dependency) -> FunctionalLinearity {
        let (linearity, call_usage) = self.linearity_call();
        FunctionalLinearity::new(dep, linearity, call_usage)
    }
    /// Get the linearity and call usage of this function kind
    #[inline]
    pub fn linearity_call(self) -> (Linearity, Usage) {
        use FnKind::*;
        match self {
            FnCopy => (Linearity::NONLINEAR, Usage::OBSERVED),
            Fn => (Linearity::AFFINE, Usage::OBSERVED),
            FnOnce => (Linearity::AFFINE, Usage::CONSUMED),
            ExCopy => (Linearity::RELEVANT, Usage::UNUSED),
            Fx => (Linearity::RELEVANT, Usage::OBSERVED),
            Ex => (Linearity::LINEAR, Usage::UNUSED),
            ExOnce => (Linearity::LINEAR, Usage::CONSUMED),
            FxOnce => (Linearity::LINEAR, Usage::USED),
        }
    }
    /// Get the linearity of this function kind
    #[inline]
    pub fn linearity(self) -> Linearity {
        self.linearity_call().0
    }
    /// Get the call usage of this function kind
    #[inline]
    pub fn call_usage(self) -> Usage {
        self.linearity_call().1
    }
}

/// A lambda function
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Lambda(pub Option<FnKind>, pub Parametrized);

impl Lambda {
    /// Get the maximal functional linearity of this pi type
    pub fn max_fun_lin(&self) -> FunctionalLinearity {
        self.0
            .map(|kind| kind.fun_lin(self.1.dep))
            .unwrap_or_else(|| FunctionalLinearity::new(self.1.dep, Linearity::LINEAR, Usage::USED))
    }
}

/// A pi type
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Pi(pub FnKind, pub Parametrized);

impl Pi {
    /// Get the functional linearity of this pi type
    pub fn fun_lin(&self) -> FunctionalLinearity {
        self.0.fun_lin(self.1.dep)
    }
}
