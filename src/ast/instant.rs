pub use super::*;

/// An instant, having an (optional) name and (optional) constraint
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Instant {
    /// The name of this instant
    pub name: Option<String>,
    /// The constraint of this instant
    pub constraint: Option<Constraints>,
}

/// A list of constraints on an instant or lifetime
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Constraints(pub Vec<Constraint>);

/// A constraint on an instant or lifetime
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Constraint(pub Relationship, pub String);
