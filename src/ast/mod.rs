/*!
An AST for `isotope` expressions.
*/
pub use crate::value::{Dependency, FunctionalLinearity, Linearity, Relationship, Usage, Variance};
use num::BigUint;
use smallvec::SmallVec;
use std::sync::Arc;

mod instant;
mod param;
pub use instant::*;
pub use param::*;

/// An `isotope` expression
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Expr {
    // === Basic syntax ===
    /// An identifier
    Ident(String),
    /// A hole, "_"
    Hole,
    /// A function application, i.e. S-expression
    Sexpr(Sexpr),
    /// A lambda function
    Lambda(Lambda),

    // === Fundamental typing ===
    /// A pi type
    Pi(Pi),
    /// A typing universe, at a given level
    Universe(u64),

    // === Fundamental lifetimes
    /// A lifetime optionally satisfying a given constraint
    Lifetime(Option<Constraints>),

    // === Primitives ===
    /// The empty type
    Empty,
    /// The unit type
    Unit,
    /// A natural number
    Natural(BigUint),
    /// The type of natural numbers
    Nat,
    /// A boolean value
    Boolean(bool),
    /// The type of booleans
    Bool,

    // === Syntax sugar ===
    /// A scope
    Scope(Scope),
    /// A type of expression. This is a *macro*, using *inference*, and in particular will *not* yield a typeof function!
    TypeOf(Arc<Expr>),
}

impl Expr {
    /// A convenience function to construct to an ident or a hole as an `Option<String>`, the latter represented as `None`
    ///
    /// # Examples
    /// ```rust
    /// # use isotope::ast::*;
    /// assert_eq!(Expr::ident_str("_"), None);
    /// assert_eq!(Expr::ident_str("x"), Some("x".to_owned()));
    /// ```
    pub fn ident_str<S: Into<String> + PartialEq<&'static str>>(ident: S) -> Option<String> {
        if ident.eq(&"_") {
            None
        } else {
            Some(ident.into())
        }
    }
    /// A convenience function to construct an ident, or a hole
    ///
    /// # Examples
    /// ```rust
    /// # use isotope::ast::*;
    /// assert_eq!(Expr::ident("_"), Expr::Hole);
    /// assert_eq!(Expr::ident("x"), Expr::Ident("x".to_owned()));
    /// ```
    pub fn ident<S: Into<String> + PartialEq<&'static str>>(ident: S) -> Expr {
        if let Some(ident) = Expr::ident_str(ident) {
            Expr::Ident(ident)
        } else {
            Expr::Hole
        }
    }
}

impl<'a> From<&'a str> for Expr {
    fn from(s: &'a str) -> Expr {
        Expr::ident(s)
    }
}

impl From<String> for Expr {
    fn from(s: String) -> Expr {
        Expr::ident(s)
    }
}

/// An isotope statement
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Statement {
    /// An instant having a given set of judgements
    Instant(Instant),
    /// A parameter declaration
    Param(Param),
    /// A let statement
    Let(Let),
}

/// A let statement
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Let {
    /// The name of the variable being assigned to
    pub name: String,
    /// Optionally, a judgement on the variable being assigned to
    pub judgement: Option<Judgement>,
    /// The value the variable is being assigned
    pub value: Arc<Expr>,
}

/// A typing judgement
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Judgement {
    /// This judgement's variance
    pub variance: Variance,
    /// Whether this judgement is strict
    pub strict: bool,
    /// The type being judged against
    pub ty: Arc<Expr>,
}

/// The size of a small S-expression
pub const SMALL_SEXPR_SIZE: usize = 2;

/// An S-expression
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Sexpr(pub SmallVec<[Arc<Expr>; SMALL_SEXPR_SIZE]>);

/// A scope
#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Scope {
    /// The statements in this scope
    pub statements: Vec<Statement>,
    /// The result of this scope
    pub result: Arc<Expr>,
}
