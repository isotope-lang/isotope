/*!
Parameters and parametrized expressions
*/
use super::*;

/// Parse a parameter
pub fn param(input: &str) -> IResult<&str, Param> {
    map(
        separated_pair(ident, delimited(opt(ws), tag(TYPING), opt(ws)), opt(expr)),
        |(name, ty)| Param {
            name: Expr::ident_str(name),
            ty: ty.map(Arc::new),
        },
    )(input)
}

/// Parse a parameter statement
pub fn param_stmt(input: &str) -> IResult<&str, Param> {
    delimited(
        preceded(tag(PARAM), ws),
        param,
        preceded(opt(ws), tag(STMT_SEP)),
    )(input)
}

/// Parse a parametrized value
pub fn parametrized(input: &str) -> IResult<&str, Parametrized> {
    map(
        tuple((
            param,
            preceded(opt(ws), dependency),
            preceded(opt(ws), expr),
        )),
        |(param, dep, value)| Parametrized {
            param,
            dep,
            value: Arc::new(value),
        },
    )(input)
}

/// Parse a function kind
pub fn fn_kind(input: &str) -> IResult<&str, FnKind> {
    alt((
        map(tag(FNCOPY), |_| FnKind::FnCopy),
        map(tag(FNONCE), |_| FnKind::FnOnce),
        map(tag(EXCOPY), |_| FnKind::ExCopy),
        map(tag(EXONCE), |_| FnKind::ExOnce),
        map(tag(FXONCE), |_| FnKind::FxOnce),
        map(tag(FN), |_| FnKind::Fn),
        map(tag(FX), |_| FnKind::Fx),
        map(tag(EX), |_| FnKind::Ex),
    ))(input)
}

/// Parse a pi type
pub fn pi(input: &str) -> IResult<&str, Pi> {
    map(
        separated_pair(fn_kind, opt(ws), parametrized),
        |(kind, value)| Pi(kind, value),
    )(input)
}

/// Parse a lambda function
pub fn lambda(input: &str) -> IResult<&str, Lambda> {
    map(
        separated_pair(
            preceded(
                tag(LAMBDA),
                opt(delimited(
                    preceded(tag("["), opt(ws)),
                    fn_kind,
                    preceded(opt(ws), tag("]")),
                )),
            ),
            opt(ws),
            parametrized,
        ),
        |(kind, value)| Lambda(kind, value),
    )(input)
}

#[cfg(test)]
mod test {
    use super::*;

    fn x_param() -> Param {
        Param {
            name: Some("x".to_owned()),
            ty: Some(Expr::Universe(0).into()),
        }
    }

    fn x_parametrized() -> Parametrized {
        Parametrized {
            param: x_param(),
            dep: Dependency::USED,
            value: Expr::ident("x").into(),
        }
    }

    #[test]
    fn params_parse() {
        assert_eq!(param("x: #universe[0]"), Ok(("", x_param())))
    }

    #[test]
    fn parametrizeds_parse() {
        assert_eq!(
            parametrized("x: #universe[0] => x"),
            Ok(("", x_parametrized()))
        )
    }

    #[test]
    fn fnkinds_parse() {
        let pairs = [
            ("#fncopy", FnKind::FnCopy),
            ("#fn", FnKind::Fn),
            ("#fnonce", FnKind::FnOnce),
            ("#excopy", FnKind::ExCopy),
            ("#fx", FnKind::Fx),
            ("#ex", FnKind::Ex),
            ("#exonce", FnKind::ExOnce),
            ("#fxonce", FnKind::FxOnce),
        ];
        for &(s, k) in &pairs {
            assert_eq!(fn_kind(s), Ok(("", k)));
        }
    }

    #[test]
    fn lambdas_parse() {
        assert_eq!(
            lambda("#lambda x: #universe[0] => x"),
            Ok(("", Lambda(None, x_parametrized())))
        );
        assert_eq!(
            expr("#lambda x: #universe[0] => x"),
            Ok(("", Expr::Lambda(Lambda(None, x_parametrized()))))
        );
        assert_eq!(
            lambda("#lambda[#fnonce] x: #universe[0] => x"),
            Ok(("", Lambda(Some(FnKind::FnOnce), x_parametrized())))
        );
        assert_eq!(
            expr("#lambda[#fnonce] x: #universe[0] => x"),
            Ok((
                "",
                Expr::Lambda(Lambda(Some(FnKind::FnOnce), x_parametrized()))
            ))
        )
    }
}
