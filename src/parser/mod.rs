/*!
A parser for `isotope`'s grammar. Generates an AST from input strings.
*/
use crate::ast::*;
use crate::Arc;
use nom::branch::*;
use nom::bytes::complete::{is_a, is_not, tag};
use nom::character::{complete::*, *};
use nom::combinator::*;
use nom::multi::*;
use nom::sequence::*;
use nom::IResult;
use num::BigUint;

mod glyphs;
mod instant;
mod param;
mod primitive;
mod util;
pub use glyphs::*;
pub use instant::*;
pub use param::*;
pub use primitive::*;
pub use util::*;

/// The set of special characters
pub const SPECIAL_CHARACTERS: &str = "\n\r\t :[](){}#\"\\,;~.=*";

/// The keyword for a `let`-statement
pub const LET: &str = "#let";
/// The keyword for a parameter declaration
pub const PARAM: &str = "#param";
/// The keyword for a free instant declaration
pub const INSTANT: &str = "#instant";
/// The keyword for a polymorphic typing universe
pub const UNIVERSE: &str = "#universe";
/// The keyword for lifetimes
pub const LIFETIME: &str = "#lifetime";
/// The keyword for closures
pub const LAMBDA: &str = "#lambda";
/// The keyword for FnCopy function types
pub const FNCOPY: &str = "#fncopy";
/// The keyword for Fn function types
pub const FN: &str = "#fn";
/// The keyword for FnOnce function types
pub const FNONCE: &str = "#fnonce";
/// The keyword for ExCopy function types
pub const EXCOPY: &str = "#excopy";
/// The keyword for Fx function types
pub const FX: &str = "#fx";
/// The keyword for Ex function types
pub const EX: &str = "#ex";
/// The keyword for ExOnce function types
pub const EXONCE: &str = "#exonce";
/// The keyword for FxOnce function types
pub const FXONCE: &str = "#fxonce";
/// The keyword for a live range
pub const LIVE: &str = "#live";
/// The keyword for the beginning of a lifetime
pub const BEGIN: &str = "#begin";
/// The keyword for the end of a lifetime
pub const END: &str = "#end";
/// The empty type
pub const EMPTY: &str = "#empty";
/// The unit type
pub const UNIT: &str = "#unit";
/// The type of natural numbers
pub const NAT: &str = "#nat";
/// The type of boolean
pub const BOOL: &str = "#bool";
/// The true boolean constant
pub const TRUE: &str = "#true";
/// The false boolean constant
pub const FALSE: &str = "#false";
/// The `typeof` operator
pub const TYPEOF: &str = "#typeof";

// General punctuation

/// The symbol for an assignment
pub const ASSIGN: &str = "=";
/// The separator for statements
pub const STMT_SEP: &str = ";";
/// The symbol for a typing judgement
pub const TYPING: &str = ":";
/// The symbol for loosening a typing judgement
pub const LOOSE: &str = "~";

/// Parse an `isotope` expression
///
/// # Examples
/// ```rust
/// # use isotope::parser::expr;
/// # use isotope::ast::Expr::*;
/// // Identifiers
/// assert_eq!(expr("x").unwrap(), ("", Ident("x".to_owned())));
///
/// // Basic constants
/// assert_eq!(expr("0x52").unwrap(), ("", Natural(0x52u64.into())));
/// assert_eq!(expr("#universe[1]").unwrap(), ("", Universe(1)));
/// ```
pub fn expr(input: &str) -> IResult<&str, Expr> {
    alt((
        // Basic expressions
        map(ident, Expr::ident),
        map(sexpr, Expr::Sexpr),
        map(lambda, Expr::Lambda),
        primitive,
        // Type theory
        map(pi, Expr::Pi),
        map(universe, Expr::Universe),
        // Lifetimes
        map(lifetime, Expr::Lifetime),
        // Sugar
        map(scope, Expr::Scope),
        map(typeof_expr, Expr::TypeOf),
    ))(input)
}

/// Parse an `isotope` statement
pub fn statement(input: &str) -> IResult<&str, Statement> {
    alt((
        map(instant, Statement::Instant),
        map(param_stmt, Statement::Param),
        map(let_, Statement::Let),
    ))(input)
}

/// Parse an S-expression
///
/// # Examples
/// ```rust
/// # use isotope::parser::sexpr;
/// # use isotope::ast::{Expr::*, Sexpr};
/// # use smallvec::smallvec;
/// assert_eq!(
///     sexpr("(f x y z)").unwrap(),
///     ("", Sexpr(smallvec![
///         Ident("f".to_owned()).into(),
///         Ident("x".to_owned()).into(),
///         Ident("y".to_owned()).into(),
///         Ident("z".to_owned()).into()
///     ]))
/// );
/// ```
pub fn sexpr(input: &str) -> IResult<&str, Sexpr> {
    map(
        delimited(
            tag("("),
            many0(preceded(opt(ws), map(expr, Arc::new))),
            preceded(opt(ws), tag(")")),
        ),
        |input| Sexpr(input.into()),
    )(input)
}

/// Parse a scope
pub fn scope(input: &str) -> IResult<&str, Scope> {
    map(
        delimited(
            tag("{"),
            pair(
                many0(preceded(opt(ws), statement)),
                delimited(opt(ws), expr, opt(ws)),
            ),
            tag("}"),
        ),
        |(statements, result)| Scope {
            statements,
            result: Arc::new(result),
        },
    )(input)
}

/// Parse a `let` statement
pub fn let_(input: &str) -> IResult<&str, Let> {
    map(
        tuple((
            tag(LET),
            preceded(opt(ws), map_opt(ident, Expr::ident_str)),
            preceded(opt(ws), opt(judgement)),
            preceded(opt(ws), tag(ASSIGN)),
            preceded(opt(ws), expr),
            preceded(opt(ws), tag(STMT_SEP)),
        )),
        |(_, name, judgement, _, value, _)| Let {
            name,
            judgement,
            value: Arc::new(value),
        },
    )(input)
}

/// Parse a typing judgement
pub fn judgement(input: &str) -> IResult<&str, Judgement> {
    map(
        tuple((
            opt(tag(LOOSE)),
            tag(TYPING),
            opt(variance),
            preceded(opt(ws), expr),
        )),
        |(loose, _, variance, expr)| Judgement {
            variance: variance.unwrap_or(Variance::Invariant),
            strict: loose.is_none(),
            ty: Arc::new(expr),
        },
    )(input)
}

/// Parse a typeof expression
pub fn typeof_expr(input: &str) -> IResult<&str, Arc<Expr>> {
    map(preceded(tag(TYPEOF), preceded(opt(ws), expr)), Arc::new)(input)
}

/// Parse a string forming a valid `isotope` identifier
///
/// An `isotope` identifier may be any sequence of non-whitespace characters which does not
/// contain a special character. This parser does *not* consume preceding whitespace!
///
/// # Examples
/// ```rust
/// # use isotope::parser::ident;
/// assert_eq!(ident("hello "), Ok((" ", "hello")));
/// assert!(ident(" bye").is_err());
/// assert!(ident("0x35").is_err());
/// assert_eq!(ident("x35"), Ok(("", "x35")));
/// assert_eq!(ident("你好"), Ok(("", "你好")));
/// let arabic = ident("الحروف العربية").unwrap();
/// let desired_arabic = (" العربية" ,"الحروف");
/// assert_eq!(arabic, desired_arabic);
/// ```
pub fn ident(input: &str) -> IResult<&str, &str> {
    verify(is_not(SPECIAL_CHARACTERS), |ident: &str| {
        !is_digit(ident.as_bytes()[0])
    })(input)
}

/// Parse a typing universe
///
/// # Examples
/// ```rust
/// # use isotope::parser::universe;
/// assert_eq!(universe("#universe[32]"), Ok(("", 32)));
/// assert_eq!(universe("#universe   [  0 ]"), Ok(("", 0)));
/// ```
pub fn universe(input: &str) -> IResult<&str, u64> {
    preceded(
        tag(UNIVERSE),
        delimited(
            delimited(opt(ws), tag("["), opt(ws)),
            u64_dec,
            delimited(opt(ws), tag("]"), opt(ws)),
        ),
    )(input)
}

/// Parse a boolean
///
/// # Examples
/// ```rust
/// # use isotope::parser::boolean;
/// assert_eq!(boolean("#true").unwrap(), ("", true));
/// assert_eq!(boolean("#false").unwrap(), ("", false));
/// assert!(boolean("true").is_err());
/// assert!(boolean("false").is_err());
/// ```
pub fn boolean(input: &str) -> IResult<&str, bool> {
    alt((map(tag(TRUE), |_| true), map(tag(FALSE), |_| false)))(input)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn valid_expressions_parse() {
        let expressions = [
            "(+ x y)",
            "#lambda x: A => y",
            "#fn x: A => B",
            "#fncopy x: A => (B x)",
            "{ #let x = y; #let y = z; (f x y) }",
            "2562",
            "0x523",
            "(+ 24 252)",
            "(f x y (g z))",
            "#fx a : b => (R a)",
            "#fn a : b => (R a)",
            "#empty",
            "()",
            "#unit",
            "#true",
            "#false",
            "#bool",
            "#nat",
        ];
        for e in expressions.iter() {
            let (rest, parse) = expr(e).unwrap();
            assert_eq!(
                rest, "",
                "Expression {:?} not completely parsed! Parse = {:#?}",
                e, parse
            )
        }
    }

    #[test]
    fn invalid_expressions_dont_parse() {
        let expressions = [
            "(+ x y",
            "#lambda x =>",
            "#lambda x : A =>",
            "#lambda x =>",
            "#fn x =>",
            "#begin[(f x y)]",
            "#notakeyword",
            "#let",
            "#let x = y",
            "#let x = y;",
            "{ #let x = y; }",
            "{ #let x = y; x",
        ];
        for e in expressions.iter() {
            let parse = expr(e);
            assert!(
                parse.is_err(),
                "Parse for {:?} should be an error, but got {:#?}",
                e,
                parse
            )
        }
    }

    #[test]
    fn judgements_parse() {
        let judgements = [": a", ":< a", ":* b", "~: c"];
        for &j in &judgements {
            let (rest, _parse) = judgement(j).expect("Valid judgements");
            assert_eq!(rest, "");
        }
    }
}
