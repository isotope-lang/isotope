/*!
Parse instants and constraints
*/
use super::*;

/// Parse a free instant
pub fn instant(input: &str) -> IResult<&str, Instant> {
    map(
        delimited(
            preceded(tag(INSTANT), ws),
            separated_pair(ident, opt(ws), opt(constraints)),
            preceded(opt(ws), tag(STMT_SEP)),
        ),
        |(id, constraint)| Instant {
            name: Expr::ident_str(id),
            constraint,
        },
    )(input)
}

/// Parse a lifetime
///
/// # Examples
/// ```rust
/// use isotope::{parser::*, ast::*};
/// const NO_CNSTR: Constraints = Constraints(vec![]);
/// assert_eq!(lifetime("#lifetime").unwrap(), ("", None));
/// assert_eq!(lifetime("#lifetime{}").unwrap(), ("", Some(NO_CNSTR)));
/// ```
pub fn lifetime(input: &str) -> IResult<&str, Option<Constraints>> {
    preceded(tag(LIFETIME), opt(constraints))(input)
}

/// Parse a set of constraints on a lifetime or instant
///
/// # Examples
/// ```rust
/// # use isotope::{*, parser::*, ast::*};
/// use value::relationships::*;
/// assert_eq!(constraints("{}").unwrap(), ("", Constraints(vec![])));
/// let test_constraints = Constraints(vec![
///     Constraint(LE, "x".to_owned()),
///     Constraint(EQ, "y".to_owned()),
///     Constraint(NE, "z".to_owned())
/// ]);
/// assert_eq!(constraints("{<= x, == y, != z}").unwrap(), ("", test_constraints));
/// ```
pub fn constraints(input: &str) -> IResult<&str, Constraints> {
    map(
        delimited(
            preceded(tag("{"), opt(ws)),
            separated_list0(delimited(opt(ws), tag(","), opt(ws)), constraint),
            preceded(opt(ws), tag("}")),
        ),
        Constraints,
    )(input)
}

/// Parse an individual constraint on a lifetime or instant
pub fn constraint(input: &str) -> IResult<&str, Constraint> {
    map(
        separated_pair(relationship, opt(ws), ident),
        |(relationship, ident)| Constraint(relationship, ident.to_owned()),
    )(input)
}