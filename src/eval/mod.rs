/*!
Evaluation for `isotope` values
*/
#![allow(dead_code)]
use crate::error::*;
use crate::value::*;
use fxhash::FxHashMap as HashMap;
use std::collections::hash_map::Entry;

/// A context for the evaluation of `isotope` values
///
/// May be cheaply (O(1)) cloned, for use in a semi-immutable fashion.
#[derive(Debug, Eq, Default)]
pub struct EvalCtx {
    /// The symbol definition set
    symbols: HashMap<SymbolId, ValId>,
    /// The relative evaluation cache
    cache: HashMap<ValId, ValId>,
    /// Whether this context is normalizing
    normalizing: bool,
}

impl EvalCtx {
    /// Create a new evaluation context
    pub fn new(normalizing: bool) -> EvalCtx {
        EvalCtx {
            normalizing,
            ..EvalCtx::default()
        }
    }
    /// Get the definition of a symbol
    pub fn get(&self, symbol: &SymbolId) -> Option<&ValId> {
        self.symbols.get(symbol)
    }
    /// Check whether this free variable set is modified by this evaluation context
    pub fn is_subst(&self, symbols: &SymbolSet) -> bool {
        self.symbols.keys().any(|symbol| symbols.contains(symbol))
    }
    /// Check whether this free variable set is modified by this evaluation context
    pub fn is_not_subst(&self, symbols: &SymbolSet) -> bool {
        self.symbols.keys().all(|symbol| !symbols.contains(symbol))
    }
    /// Get the cached evaluation for a `ValId`
    pub fn cached_eval(&self, value: &ValId) -> Option<&ValId> {
        self.cache.get(value)
    }
    /// Cache an evaluation
    pub(crate) fn cache_eval(&mut self, value: ValId, eval: ValId) {
        let code = eval.code();
        let old = self.cache.insert(value, eval);
        if let Some(old) = old {
            debug_assert_eq!(old.code(), code)
        }
    }
    /// Flush the cache
    pub fn flush_cache(&mut self) {
        self.cache.clear()
    }
    /// Define a mapping, flushing the cache if any changes were made
    pub(crate) fn def_flush(
        &mut self,
        symbol: SymbolId,
        value: ValId,
    ) -> Result<(Match, bool), Error> {
        let m = symbol.match_symbol(&value)?;
        let change = self.def_flush_unchecked(symbol, value);
        Ok((m, change))
    }
    /// Define a mapping, flushing the cache if any changes were made
    pub(crate) fn def_flush_unchecked(&mut self, symbol: SymbolId, value: ValId) -> bool {
        let symbol_is_value = symbol == value;
        let change = match self.symbols.entry(symbol) {
            Entry::Occupied(mut o) => {
                if *o.get() == value {
                    false
                } else {
                    o.insert(value);
                    true
                }
            }
            Entry::Vacant(v) => {
                if symbol_is_value {
                    false
                } else {
                    v.insert(value);
                    true
                }
            }
        };
        if change {
            self.flush_cache()
        }
        change
    }
    /// Set whether this context is normalizing
    pub fn set_normalizing(&mut self, normalizing: bool) {
        if self.normalizing != normalizing {
            self.normalizing = normalizing;
            self.flush_cache()
        }
    }
    /// Get whether this context is empty
    pub fn is_empty(&self) -> bool {
        self.symbols.is_empty()
    }
    /// Get whether this context is normalizing
    pub fn is_normalizing(&self) -> bool {
        self.normalizing
    }
    /// Get whether this is the null context, i.e. empty and non-normalizing
    pub fn is_null(&self) -> bool {
        self.is_empty() && !self.is_normalizing()
    }
    /// Get this evaluation context, with a symbol added. Return `None` if no changes were made, and return an error on a typing mismatch
    pub fn added(&self, symbol: SymbolId, value: ValId) -> Result<(Match, Option<EvalCtx>), Error> {
        symbol
            .match_symbol(&value)
            .map(|m| (m, self.added_unchecked(symbol, value)))
    }
    /// Get this evaluation context, with a symbol added. Return `None` if no changes were made
    pub(crate) fn added_unchecked(&self, symbol: SymbolId, value: ValId) -> Option<EvalCtx> {
        //TODO: optimize, FIXME
        /*
        #[cfg(debug_assertions)]
        {
            let m = symbol.match_symbol(&value);
            debug_assert!(
                m.is_ok(),
                "Symbol {:#?}: {:#?} failed to match value {:#?}: {:#?}, with match {:?}",
                symbol,
                symbol.ty(),
                value,
                value.ty(),
                m
            );
        }
        */
        if let Some(old_value) = self.symbols.get(&symbol) {
            if *old_value == value {
                return None
            }
        } else if symbol == value {
            return None
        }
        let mut symbols = self.symbols.clone();
        symbols.insert(symbol, value);
        Some(EvalCtx {
            symbols,
            cache: HashMap::default(),
            normalizing: self.normalizing,
        })
    }
    /// Get this evaluation context, with a symbol removed. Return `None` if no changes were made
    pub fn removed(&self, symbol: &SymbolId) -> Option<EvalCtx> {
        //TODO: optimize
        if !self.symbols.contains_key(symbol) {
            return None;
        }
        let mut symbols = self.symbols.clone();
        symbols.remove(symbol);
        Some(EvalCtx {
            symbols,
            cache: HashMap::default(),
            normalizing: self.normalizing,
        })
    }
}

impl PartialEq for EvalCtx {
    fn eq(&self, other: &EvalCtx) -> bool {
        self.normalizing == other.normalizing && self.symbols == other.symbols
    }
}

impl Clone for EvalCtx {
    fn clone(&self) -> EvalCtx {
        // Don't copy the cache, to make cloning cheap
        EvalCtx {
            symbols: self.symbols.clone(),
            cache: HashMap::default(),
            normalizing: self.normalizing,
        }
    }
}
