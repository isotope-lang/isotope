/*!
# `isotope`

`isotope` is an experimental interpreted programming language which implements a system of temporal dependent types.
The main goal of this project is to experiemnt with design, ergonomics, and inference algorithms in the simpler setting of a fully
interpreted language before moving on to the statically-compiled `rain` intermediate representation project.
*/
#![forbid(missing_docs, missing_debug_implementations)]
#![warn(clippy::all)]
use ahash::AHasher;

pub mod ast;
pub mod builder;
pub mod error;
pub mod eval;
pub mod parser;
pub mod util;
pub mod value;

use util::*;

/// The atomic reference counting type used by this library
pub type Arc<T> = std::sync::Arc<T>;
