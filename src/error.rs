/*!
`isotope` errors
*/
use crate::value::{Linearity, Relationship, Dependency, Usage};
use failure_derive::Fail;

/// `isotope` errors
#[derive(Debug, Copy, Clone, Fail, Eq, PartialEq, Hash)]
pub enum Error {
    /// An undefined symbol
    #[fail(display = "Undefined symbol")]
    UndefinedSymbol,
    /// An identifier is defined, but not a symbol
    #[fail(display = "Identifier does not refer to a symbol")]
    NotASymbol,
    /// A value is not a valid type
    #[fail(display = "Value is not a type")]
    NotAType,
    /// A value is not a valid instant
    #[fail(display = "Value is not an instant")]
    NotAnInstant,
    /// A value is not a valid function
    #[fail(display = "Value is not a function")]
    NotAFunction,
    /// A value is not a valid function type
    #[fail(display = "Value is not a function type")]
    NotAFunctionType,
    /// A malformed parameter declaration
    #[fail(display = "Malformed parameter declaration")]
    MalformedParam,
    /// A type mismatch
    #[fail(display = "Type mismatch")]
    TypeMismatch,
    /// Variance needs to be covariant
    #[fail(display = "Variance needs to be covariant")]
    NeedsCovar,
    /// Variance needs to be contravariant
    #[fail(display = "Variance needs to be contravariant")]
    NeedsContravar,
    /// A mismatch of universe levels
    #[fail(display = "Universe level mismatch")]
    UniverseLevelMismatch,
    /// A contradictory constraint set where not expected
    #[fail(display = "Unexpected contradictory constraint-set")]
    Contradiction,
    /// A cycle causing a contradiction
    #[fail(display = "Instant order cycle")]
    Cycle,
    /// An affine value is used twice
    #[fail(display = "An affine value is used twice, i.e. a double-use error")]
    DoubleUse(Usage, Usage),
    /// Incompatible type linearities
    #[fail(display = "Incompatible type linearities {:?} and {:?}", 0, 1)]
    IncompatibleTypeLinearity(Linearity, Linearity),
    /// Incompatible call linearities
    #[fail(display = "Incompatible call usages {:?} and {:?}", 0, 1)]
    IncompatibleCallUsage(Usage, Usage),
    /// Type linearity too weak
    #[fail(display = "Type linearity too weak: !({:?} <= {:?})", 0, 1)]
    TypeLinearityTooWeak(Linearity, Usage),
    /// Incompatible linearities
    #[fail(display = "Incompatible linearities {:?}", 0)]
    IncompatibleLinearity(Option<(Linearity, Linearity)>),
    /// Wrong linearity ordering
    #[fail(display = "Wrong linearity ordering")]
    WrongLinearityOrder,
    /// Incompatible symbol dependencies
    #[fail(display = "Incompatible symbol dependencies {:?} and {:?}", 0, 1)]
    IncompatibleDependencies(Dependency, Dependency),
    /// Incompatible relationships
    #[fail(display = "Incompatible relationships {:?} and {:?}", 0, 1)]
    IncompatibleRelationships(Relationship, Relationship),
    /// A custom error message
    #[fail(display = "{}", 0)]
    Message(&'static str),
}
