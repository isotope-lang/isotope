use super::*;

/// The `isotope` repl itself
#[derive(Debug, Clone)]
pub struct Repl {
    /// The number of commands handled
    handled: usize,
    /// The builder state for this repl
    builder: Builder,
}

/// An `isotope` repl error
#[derive(Debug, Clone, Fail)]
pub enum ReplError {
    /// An error building an `isotope` value
    #[fail(display = "Value building error: {}", 0)]
    BuildError(IsotopeError),
}

impl From<IsotopeError> for ReplError {
    fn from(err: IsotopeError) -> ReplError {
        ReplError::BuildError(err)
    }
}

use ReplError::*;

impl Repl {
    /// Construct a new `isotope` repl
    pub fn new() -> Repl {
        Repl {
            handled: 0,
            builder: Builder::new(),
        }
    }
    /// Handle a command
    pub fn handle(&mut self, command: &Command) -> Result<(), ReplError> {
        match command {
            Command::Expr(e) => self.handle_expr(e)?,
            Command::Statement(s) => self.handle_statement(s)?,
            Command::Fv(e) => self.handle_fv(e)?,
            Command::Lc(e) => self.handle_lc(e)?,
            Command::Ic(e) => self.handle_ic(e)?,
        }
        self.handled += 1;
        Ok(())
    }
    /// Handle an expression
    ///
    /// Does not increment the handled count
    pub fn handle_expr(&mut self, expr: &Expr) -> Result<(), ReplError> {
        println!("Building expr: {:#?}", expr);
        let value = self.builder.expr(&expr)?;
        println!("Value: {:#?}", value);
        if let Some(normal) = value.normalized() {
            println!("Normal form: {:#?}", normal)
        }
        Ok(())
    }
    /// Handle a statement
    ///
    /// Does not increment the handled count
    pub fn handle_statement(&mut self, statement: &Statement) -> Result<(), ReplError> {
        self.builder.statement(&statement).map_err(BuildError)
    }
    /// Handle a free variable check
    ///
    /// Does not increment the handled count
    pub fn handle_fv(&mut self, expr: &Expr) -> Result<(), ReplError> {
        let value = self.builder.expr(&expr)?;
        println!("Free variables: {:#?}", value.fv());
        Ok(())
    }
    /// Handle a local constraint-set check
    ///
    /// Does not increment the handled count
    pub fn handle_lc(&mut self, expr: &Expr) -> Result<(), ReplError> {
        let value = self.builder.expr(&expr)?;
        println!("Local constraints: {:#?}", value.local_constraints());
        Ok(())
    }
    /// Handle a constancy check
    pub fn handle_ic(&mut self, expr: &Expr) -> Result<(), ReplError> {
        let value = self.builder.expr(&expr)?;
        println!("#{:?}", value.is_const());
        Ok(())
    }
}
