use clap::{App, Arg};
use failure_derive::Fail;
use isotope::ast::*;
use isotope::builder::*;
use isotope::error::Error as IsotopeError;
use isotope::parser::*;
use isotope::value::*;
use nom::branch::*;
use nom::bytes::complete::*;
use nom::combinator::*;
use nom::multi::*;
use nom::sequence::*;
use nom::IResult;
use rustyline::error::ReadlineError;
use rustyline::hint::{Hinter, HistoryHinter};
use rustyline::validate::{ValidationContext, ValidationResult, Validator};
use rustyline::{Context, Editor};
use rustyline_derive::{Completer, Helper, Highlighter};

mod command;
mod helper;
mod repl;
pub use command::*;
pub use helper::*;
pub use repl::*;

/// Run the `isotope` REPL
pub fn main() {
    let matches = App::new("isotope-repl")
        .version("0.0.0")
        .author("Jad Ghalayini, Qingyuan Qie")
        .about("A REPL for interacting and experimenting with the isotope API")
        .arg(
            Arg::with_name("history")
                .short("h")
                .long("history")
                .value_name("FILE")
                .help("Sets a file to load history from")
                .takes_value(true),
        )
        .get_matches();
    println!("isotope-repl 0.0.0");
    let history = matches.value_of("history");
    let mut editor = Editor::<IsotopeHelper>::new();
    if let Some(history) = history {
        if let Err(err) = editor.load_history(history) {
            println!("No history loaded from {}: {}", history, err)
        } else {
            println!("Successfully loaded history from {}", history)
        }
    } else {
        println!("No history loaded")
    }
    let helper = IsotopeHelper::new();
    let mut repl = Repl::new();
    editor.set_helper(Some(helper));
    loop {
        let line = editor.readline(">> ");
        match line {
            Ok(line) => {
                let mut line = line.as_str();
                while let Ok((rest, command)) = preceded(opt(ws), command)(line) {
                    if let Err(err) = repl.handle(&command) {
                        println!(
                            "Error handling command:\nCOMMAND: {:#?}\nERRORL:{:#?}",
                            command, err
                        )
                    }
                    line = rest;
                }
            }
            Err(err) => {
                println!("{}", err);
                break;
            }
        }
    }
}
