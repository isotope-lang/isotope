pub use super::*;

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Command {
    /// An `isotope` expression
    Expr(Expr),
    /// An `isotope` statement
    Statement(Statement),
    /// Print the free-variable set of an expression
    Fv(Expr),
    /// Print the local constraint set of an expression
    Lc(Expr),
    /// Print whether an expression is a constant
    Ic(Expr),
}

/// Parse a command
pub fn command(input: &str) -> IResult<&str, Command> {
    alt((
        map(expr, Command::Expr),
        map(statement, Command::Statement),
        map(
            preceded(delimited(opt(ws), tag("#fv"), opt(ws)), expr),
            Command::Fv,
        ),
        map(
            preceded(delimited(opt(ws), tag("#lc"), opt(ws)), expr),
            Command::Lc,
        ),
        map(
            preceded(delimited(opt(ws), tag("#ic"), opt(ws)), expr),
            Command::Ic,
        ),
    ))(input)
}
