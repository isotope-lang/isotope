use isotope::builder::Builder;
use isotope::value::*;

#[test]
fn id_nat_to_nat() {
    let mut builder = Builder::default();
    let (_, id_n) = builder.parse_expr("#lambda n: #nat => n").unwrap();
    assert_eq!(*id_n.local_constraints(), Constraints::default());
}
